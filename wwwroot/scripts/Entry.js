var _Nimbb;
var _Guid = "";
var id;
var start = false;
var stop = false;
var ready_cnt = 0;
var startAgain = false;

$(document).ready(function () {
  
    $('#startRecording1').css('cursor', 'default');
    $('#uploadRecording1').css('cursor', 'default');
    $('#startRecording1').addClass('record-opacity');

});

function resetRecorder()
{
    $('#startRecording1').css('cursor', 'default');
    $('#startRecording1').attr('readonly', true);

    $('#uploadRecording1').css('cursor', 'default');
    $('#uploadRecording1').attr('readonly', true);

    $('#uploadRecording1').addClass('gradBG-b4startRecordingYellow');
    $('#uploadRecording1').removeClass('gradBG-endRecordingYellow');

    $('#startRecording1').addClass('gradBG-b4startRecordingBlack');
    $('#startRecording1').removeClass('gradBG-progRecordingBlack');

    $('#startRecording1').html('<div style="margin-left: 40px !important;"></div>&nbsp;&nbsp;&nbsp;Start Recording');
     _Nimbb;
     _Guid = "";
     id;
     start = false;
     stop = false;
     ready_cnt = 0;
     startAgain = false;
     clearTimeout(_Timer);
     _Count = 0;

     $('#timer').html('<span id="min">0</span>:<span id="sec">00</span>');
    refreshplayer();
}

function customStart() {
    //if (_Count >= 60) {

    //    alert('You have already finished 60 seconds of recording');
    //    return;
    //}
    if ($('#startRecording1').attr('readonly') == true) {
        
    }
    else if (startAgain) {
        $('#startRecording1').addClass('record-opacity');
        $('#startRecording1').removeClass("active-startrecording-btn"); //add
        startAgain = false;
        clearTimeout(_Timer);
       
        resetRecorder();        
    }
    else if (start == false) {
        Timer_Count();
        $('#startRecording1').val('Stop Recording');
       
        start = true;
        Nimbb_stateChanged('nimbb', 'recording');
    }
    else {
        clearTimeout(_Timer);
        $('#uploadRecording1').attr('readonly',false);
        $('#startRecording1').val('Start Again');

        $('#uploadRecording1').css('cursor', 'pointer');
        $('#uploadRecording1').removeClass('gradBG-b4startRecordingYellow');
        $('#uploadRecording1').addClass('gradBG-endRecordingYellow');
        stop = true;
        start = false;
        startAgain = true;
        Nimbb_stateChanged('nimbb', 'recording');
    }



}

function changePlayerControl(isPlay)
{
    if (isPlay)
    {
        //customplay();
        $('#playPauseVideo').css({ "background-image": "url('/images/icons/play_dark.png')" });
        $('#playPauseVideo').attr("onclick", "customplay()");
        
    }
    else {
        $('#playPauseVideo').css({ "background-image": "url('/images/icons/pause_dark.png')" });
        $('#playPauseVideo').attr("onclick", "_Nimbb.stopVideo(); return false;");
       // _Nimbb.stopVideo();
       
    }
    return false;
}

function customplay() {
    if ($('#startRecording1').attr('readonly') == true) {

    } else {
        $('#playPauseVideo').show();
        changePlayerControl(true);

        $('#timer, #timer1').hide();
        //Nimbb_playbackStarted('nimbb');
        _Nimbb.playVideo();
    }
}

function updateText() {
    if (_Nimbb.getState() == 'ready') {
        ready = true;
        ready_cnt++;
        $('#startRecording1').removeClass('record-opacity'); //add  
        $('#startRecording1').addClass("active-startrecording-btn"); //add
        if (ready_cnt == 1) {

            $('#startRecording1').removeClass('gradBG-b4startRecordingBlack');
            $('#startRecording1').addClass('gradBG-progRecordingBlack');

            $('#startRecording1').css('cursor', 'pointer');
            $('#startRecording1 div').css(' background-color', '#ff0000 !important');
            $('#startRecording1').attr('readonly',false);
            $('#timer,#timer1').show();
        }
    }
    if (_Nimbb.getMode() == "record" && ready && start & !stop) {
        _Nimbb.recordVideo();
    }
    if (stop) {
        stop = false;
        _Nimbb.stopVideo();
    }
}


function submitvideo() {
    _Nimbb.stopVideo();
    setTimeout(
  function() 
  {
      _Nimbb.saveVideo();
    //do something special
  }, 2000);
    
    //Nimbb_videoSaved("nimbb");

}


function Nimbb_playbackStopped(idPlayer) {
    changePlayerControl(true);
}

function Nimbb_playbackStarted(idPlayer) {
    changePlayerControl(false);

    //_Nimbb.playVideo();
    $('.container-main, #bottom-copy').show();
    $('#top-copy, #buttonsRecording, .recordOrnPos, #bCopy, #SelectedVideo,#uploadVideo,#topCancelButton').hide();
    $('.brief-title').html('Entry Details');
    $('.container-video').css('min-height', '0');
    $('.container-video-title').css('margin-top', '30px');
}

function saveVideo(vid) {

    var videoid = vid;
    var name = $("#applicationName").val();
    $.ajax({
       
        type: "POST",
        contentType: "application/x-www-form-urlencoded",
        url: subdirectoryName + "/tomatomaster/register/SubmitEntry/?entryName=" + encodeURIComponent(name) + "&memberId=0&entryType=video&entryDescription=&entryUrl=" + videoid,
        xhrFields: {
            onprogress: function (e) {
                if (e.lengthComputable) {
                    var percentComplete = e.loaded / e.total;
                    //Do something with upload progress
                    console.log(percentComplete * 50);
                    $('#submitting').css('width', (percentComplete * 50) + 'px');
                }
            }
        },
        success: function (data) {

            $('#submitting').css('width', '213px');
            setTimeout(function () {
                window.location = data.Value;
            }, 300);
            //$('.uploadEntryOrnament').css('padding-top', '178px');
            //$('#cancel').hide();
           // window.location = data.Value;
            },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
}

// Start playback.
function play() {
    _Nimbb.playVideo(true);

}

// Stop playback.
function stop() {
    _Nimbb.stopVideo();
}


var _Timer;
var _Count = 0;

// Constant for maximum recording time (in seconds).
var MAX_TIME = 60;

// Event: Nimbb Player has been initialized and is ready.
function Nimbb_initCompleted(idPlayer) {
     id = idPlayer;
    // Get a reference to the player since it was successfully created.
    _Nimbb = document[idPlayer];


    $('#nimbbLoader').hide();
}


// Event: the state of the player changed.
function Nimbb_stateChanged(idPlayer, state) {
    // Update button text.
    updateText();
}

// Event: the video was saved.
function Nimbb_videoSaved(idPlayer) {
    
    //alert("Nimbb_videoSaved called");
    // Get video GUID.
    _Guid = _Nimbb.getGuid();

   if (_Guid == null) {
        // console.log('guid null');

        //Nimbb_videoSaved("nimbb");
    }
    else {
        var vid = _Guid;

         saveVideo(vid);
    }
   
}

function Nimbb_noCameraDetected(idPlayer) {
    //  console.log("No cammera");
    $("#clicknocamera").click();
}

// Event: the timer count.
function Timer_Count() {
    // Decrement total count and check if we have reached the maximum time.
    _Count++;

    if (_Count < 10) {
        $('#timer #sec').html('0' + _Count);
    }
    else {
        if (_Count == 60) {

            $('#startRecording1').click();
            //$('#timer #min').html('1');
            //$('#timer #sec').html('00');

            //stop = false;
            //_Nimbb.stopVideo();
            //Nimbb_stateChanged('nimbb', 'recording');
            //$('#uploadRecording1').removeAttr('disabled');
            //$('#uploadRecording1').css('cursor', 'pointer');
            //$('#uploadRecording1').removeClass('gradBG-b4startRecordingYellow');
            //$('#uploadRecording1').addClass('gradBG-endRecordingYellow');


            return;
        }
        $('#timer #sec').html(_Count);
    }


    // Update button text.
    // updateText();

    // Let's continue the timer.
    _Timer = setTimeout("Timer_Count()", 1000);
}

// Called when user clicks the link.
function action() {


    // console.log('action() called');
    // Check player's state and call appropriate action.
    if (_Nimbb.getState() == "recording") {
        stop();
    }
    else {
        record();
    }
}

// Start recording the video.
function record() {
    // Make sure the user is not already recording.
    if (_Nimbb.getState() == "recording") {
        //console.log("You are already recording a video.");
        return;
    }

    // Prepare timer object.
    _Count = MAX_TIME + 1;
    Timer_Count();

    // Start recording.
    _Nimbb.recordVideo();
}

// Stop recording the video.
function stop() {
    // Make sure the user is recording.
    if (_Nimbb.getState() != "recording") {
        //console.log("You need to record a video.");
        return;
    }

    // Stop timer.
    clearTimeout(_Timer);

    // Stop recording.
    _Nimbb.stopVideo();
}
var ready = false;


function refreshplayer() {
    $('#nimbbLoader').show();
    var builder = '';
    builder = '<object id="nimbb" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="500"" height="275" codebase="http://fpdownload.macromedia.com/get/flashplayer/current/swflash.cab">' +
    '<param name="movie" value="http://player.nimbb.com/nimbb.swf?lang=en&mode=record&recordlength=6000&showlabels=0&quality=10&fps=15&showicons=0&showcounter=0&audioquality=5&nologo=1&key=80f645bbff" />' +
    '<param name="allowScriptAccess" value="always" />' +
    '<param name="allowFullScreen" value="true" />' +
    '<embed name="nimbb" src="http://player.nimbb.com/nimbb.swf?lang=en&mode=record&recordlength=6000&showlabels=0&quality=10&fps=15&showicons=0&showcounter=0&audioquality=5&key=80f645bbff&showmenu=0&playonclick=0" width="500"" height="275" allowScriptAccess="always" allowFullScreen="true" pluginspage="http://www.adobe.com/go/getflashplayer" type="application/x-shockwave-flash"></embed>' +
              '</object>';
    $("#myvideo1").html(builder);
}