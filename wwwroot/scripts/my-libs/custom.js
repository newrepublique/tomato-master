$(document).ready(function () {

    AssignTextHintLiClass();

    //Home Page Footer Box Slide down
    $('.boxwrapper').each(function(){
        var _this = $(this);
        _this.hoverIntent({
            over: function(){
                _this.stop().animate({'bottom': '-2px'}, 250);
                _this.addClass('boxwrapper-opened').removeClass('boxwrapper-closed');
            },
            out: function(){
                _this.stop().animate({'bottom': '-244px'}, 250);
                _this.removeClass('boxwrapper-opened').addClass('boxwrapper-closed');
            }
        });
    });

    //jQuery Accordion Funciton and append H3

    $('.fancybox').fancybox();
    $('.fancybox2').fancybox({

        width: 840,
        height: 550,
        autoSize: false,
        autoHeight: false,
        autoWidth: false
    });

    $('form.form').jqTransform({
        imgPath: 'images/form/'
    });


    //cuffon fonts 
   /* Cufon.replace('#accordion dt, .product-text h3, .product-text span.to, h3, .boxcontent h4, p.devider span, button.button span, .recipe-name, .description p.title, .onload-show .story-year, .onload-hide .story-year, .year-text, .entry-content p, .entry-title, .trip-part .title-text', {
        fontFamily: 'Berthold Akzidenz-Grotesk',
        fontWeight: 400,
        hover: true
    });
  
    Cufon.replace('.fnav-inner ul li a.link, .boxwrapper .linkclose a.fnlink, #secondary-menu ul li a, h3 span.bold', {
        fontFamily: 'Berthold Akzidenz-Grotesk',
        fontWeight: 700,
        hover: true
    });

    Cufon.replace('nav, .social-link a span', {
        fontFamily: 'Berthold Akzidenz-Grotesk',
        fontWeight: 700,
        hover: false,
        textShadow: '#9e131b 2px 2px'
    });
	
	Cufon.replace('.product-text h3',{
        fontFamily: 'Berthold Akzidenz-Grotesk',
        fontWeight: 400,
        hover: true,
		textShadow: '0 2px 5px #999'
    });

    Cufon.replace('h1, span.largetext', {
        fontFamily: 'Youngblood',
        fontWeight: 400,
        hover: true,
		textShadow: '1px 1px #ccc'

    });
  
    Cufon.replace('h1.title.yat', {
        fontFamily: 'Youngblood Alternate Two',
        fontWeight: 400,
        hover: true

    });*/
	


});

$(window).load(function () {

    $("#spot_light li").hover(function () {
        $(".wrapper").addClass("spotlight_over");
        $(".spotlight_over").css({
            "height": $(window).height()
        });
        $("#spot_light li").css("opacity", 0.8);
        $(this).css("opacity", 1);
        var ul_left = $(".product_slider .mCSB_container").position().left;
        //alert(ul_left);
        var li_pos = $(this).position().left;
        var li_width = $(this).width();
        $(".spotlight_over").css("background-position", -2490 + (li_pos + (li_width / 2)) + ul_left);


    }, function () {
        $(".wrapper, .inside").removeClass("spotlight_over");
        $("#spot_light li").css("opacity", 1);
    });

    //$('#slider').nivoSlider({});
	
	$(".entry_slider ul li .txtbox, .entry_slider ul li .imgbox, .entry_slider ul li .vidbox").hover(function(){
	    $(this).find(".entry-content").fadeIn(50);
	}, function () {
	    $(this).find(".entry-content").fadeOut(50);
	});
	
	
	$("#our-story-container ul li a").hover(function(){
	
	$(this).find(".onload-hide").fadeIn(50);									
	},function(){
		
	$(this).find(".onload-hide").fadeOut(50);									
		
	});
	
	
	
	//$(".recipe-content").hover(function(){
									   
	//$(this).find(".recipe-other-details").slideDown(400);
	
	//},function(){
		
	//$(this).find(".recipe-other-details").slideUp(400);
		
	//});
	
	//$(window).scroll(function () {
	//	var scroll_pos = $(window).scrollTop();
	//    $(".footer-nav").css("bottom", (scroll_pos * -1) + 22);
	//});

});

function AssignTextHintLiClass()
{
    $("li:first-child,td:first-child,dl:first-child, dd:first-child,dt:first-child,ul:first-child,tr:first-child,th:first-child").addClass("first");
    $('li:last-child,td:last-child,dl:last-child, dd:last-child,dt:last-child,ul:last-child,tr:last-child,th:last-child').addClass('last');

    $('input:text, input:password, textarea').each(function () {
        var default_value = this.value;
        $(this).focus(function () {
            if (this.value == default_value) {
                this.value = '';
            }
        });

        $(this).blur(function () {
            if (this.value == '') {
                this.value = default_value;
            }
        });

    });

}