
var entryPreviewId = "";
var entryPreviewHref = "";
var userDefaultCommentImage = "/ImageGen.ashx?width=34&height=34&bgcolor=222222&pad=true&image=/images/Mutti_default_avatar_grey.jpg";


//function GetRequiredEntryIdURL() {
//    var url = window.location.href;
//    url = url.split('/');
//    url = url[url.length - 1];

//    if (url.indexOf('content-box') == 1) {
//        //get href
//        entryPreviewHref = url.substring(1, url.length);
//        //get id
//        url = url.split('_');
//        entryPreviewId = url[url.length - 1];
//    }
//}

$(document).ready(function () {
    SubmitUserComment();
    $(".image-label").click(function () {
        //var test = $("fb-like fb_edge_widget_with_comment fb_iframe_widget").html();
        //alert(test);
    });

    $(".got-it-button").click(function () {

        var userPicture = "http://graph.facebook.com/" + id + "/picture";

        //check - profile image - new entry
        if ($.trim(id).length == 0)
            userPicture = userDefaultCommentImage;

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: subdirectoryName + "/tomatomaster/Comment/RegForComment",
            data: {
                FirstName: $("#fname").val(),
                LastName: $("#lname").val(),
                Email: $("#email").val(),
                Picture: userPicture
            },
            success: function (data) {

                if ($.trim(authorName).length == 0)
                    authorName = $("#fname").val() + " " + $("#lname").val();

                if ($.trim(authorPicture).length == 0)
                    authorPicture = "http://graph.facebook.com/" + id + "/picture";

                $(".fancybox-close").click();

                isCodeEntryView = true;

                UploadComment(commid, commessage);
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    });

});

function SubmitUserComment() {

    $(".submitComment").click(function () {

        commid = $(this).attr("id").replace("submitComment-", "");
        commessage = $("#txt-" + commid).val();
        if (($.trim(commessage).length > 0) && $.trim(commessage) != "Leave comment..") {
            UploadComment(commid, commessage);
        }
    });
}

function UploadComment(commid, commessage) {

    $.ajax({
        type: "POST",
        contentType: "application/x-www-form-urlencoded",
        url: subdirectoryName + "/mutti/comment/AddComment/",
        data:
            {
                CommentId: 0,
                CommentDescription: commessage,
                FirstName: "default",
                LastName: "default",
                Email: "default",
                EntryId: commid,
                EntriesUrl: "default",
                UserPicture: "default"
            },
        success: function (data) {

            if (data.toString().indexOf("true") != -1) {

                var tempCommId = "";

                if (isCodeEntryView)
                    $('#aCompetitionEntry_' + commid).trigger('click');

                isCodeEntryView = false;

                $(".note-" + commid).fadeIn();
                tempCommId = commid;
                setTimeout(function () {
                    $('.note-' + tempCommId + '').fadeOut();
                    $("#txt-" + tempCommId + "").val("Leave comment..");
                    tempCommId = "";
                }, 5000);

                commid = "";
                commessage = "";

            }
            else {
                $("#clicklogincomment").click();
            }

        },
        error: function (xhr, ajaxOptions, thrownError) {
            isCodeEntryView = false;
        }
    });

}

function GetCompetitionEntryComments(id) {
    var ulElement = null;
    var element = null;

    if ($('#content-box_' + id).length > 0) //image entry
        element = $('#content-box_' + id);
    else if ($('#content-box2_' + id).length > 0) //video entry
        element = $('#content-box2_' + id);
    else if ($('#content-box3_' + id).length > 0) // text entry
        element = $('#content-box3_' + id);

    if ($(element).length > 0) {
        ulElement = $(element).find('.comments-block').find('ul[data-comment-ul=1]');
    }

    $.ajax({
        type: "POST",
        contentType: "application/x-www-form-urlencoded",
        url: subdirectoryName + "/mutti/comment/FetchComment/",
        data:
            {
                entryId: id
            },
        beforeSend: function (xhr) {

        },
        success: function (data) {

            if ($(data).length > 0) {

                $(ulElement).empty();

                $.each(data, function (index, object) {

                    var date = new Date(parseInt(object.DateCreated.substr(6)));
                    var commentDate = $.datepicker.formatDate('MM dd yy', date);
                    var commentTime = date.getHours() + ":" + (date.getMinutes() < 10 ? '0' : '') + date.getMinutes();

                    //check - profile image - existing entry
                    if ((object.UserPicture).indexOf('ImageGen') != 1) {
                        var result = object.UserPicture.split('/');
                        if ($.trim(result[3]).length == 0)
                            object.UserPicture = userDefaultCommentImage;
                    }

                    var html = '<li> ' +
  '<div class="avatar">' +
            '<img width="100%" src=\'' + object.UserPicture + '\' alt="" />' +
        '</div>' +
        '<p class="author">' + object.FirstName + ' ' + object.LastName + '</p>' +
        '<p class="comment">' + object.CommentDescription + '</p>' +
  '<span class="date">' + commentDate + ' at ' + commentTime + '</span>' +
    '</li>';

                    $(ulElement).append(html);
                });
            }

            //facebook like button render
            var fburl = $('#fbUrl_' + id + '').html();
            var fbElement = $('#divFbLike_' + id + '');
            $(fbElement).html('<fb:like href="' + fburl + '" data-send="false" data-layout="box_count" data-show-faces="false" data-font="segoe ui" />');
            FB.XFBML.parse($(fbElement).get(0));

        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });


}

