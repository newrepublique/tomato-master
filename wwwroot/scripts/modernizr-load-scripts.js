﻿

var switchTo5x = true;
ModernizrLoad();

function ModernizrLoad() {
    var cssArray = [
        "/css/normalize.min.css",
        "/css/fonts.css",
        "/css/text.css",
        "/css/gradient.css",
        "/css/jquery-ui.css",
        "/css/jqtransform.css",
        "/css/jqtransform.css",
        "/css/jquery.fancybox.css",
        "/css/jquery.mCustomScrollbar.css",
        "/css/nivo/default.css",
        "/css/nivo/nivo-slider.css"
    ];

    var jsArray = [
        "/scripts/libs/jquery-1.9.1.min.js",
        "/scripts/libs/jquery-ui.js",
        "/scripts/libs/jquery.fancybox.js",
        "/scripts/libs/jquery.jqtransform.js",
        "/scripts/libs/jquery.hoverIntent.minified.js",
        "/scripts/libs/jquery.mCustomScrollbar.concat.min.js",
        "/scripts/libs/cufon-yui.js",
        "/scripts/libs/jquery.nivo.slider.js",
        "/scripts/my-libs/Berthold_Akzidenz-Grotesk_400.font.js",
        "/scripts/my-libs/Berthold_Akzidenz-Grotesk_700.font.js",
        "/scripts/my-libs/Youngblood_400.font.js",
        "/scripts/my-libs/Youngblood_Alternate_Two_400.font.js",
        "/scripts/my-libs/custom.js",
        "../../../w.sharethis.com/button/buttons.js"
    ];

    Modernizr.load({
        load: cssArray,
        complete: function () {
            Modernizr.load({
                load: jsArray,
                complete: function () {
                    stLight.options({ publisher: "ur-dbe0bc43-a60-b887-9aad-bfd8e63833cb", doNotHash: false, doNotCopy: false, hashAddressBar: false });
                    stLight.options({ publisher: "23a50081-91a0-4d78-9032-831ce46887de", doNotHash: false, doNotCopy: false, hashAddressBar: false });

                    if (window.PIE) {
                        $('.footer, .header-inner').each(function () {
                            PIE.attach(this);
                        });
                    }
                }
            });
        }
    });

    
}
