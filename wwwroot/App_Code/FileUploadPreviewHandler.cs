﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace wwwroot.App_Code
{
    public class FileUploadPreviewHandler : System.Web.IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(System.Web.HttpContext context)
        {
            var thisV = context.Request.Files.Count > 0 ? context.Request.Files[0] : null;
            CleanUpOldVideos(context);
            if (thisV != null)
            {
                var vextension = Path.GetExtension(thisV.FileName);//.Substring(thisV.FileName.LastIndexOf('.'), 4);
                var fileSize = thisV.ContentLength;
                if (
                    (vextension.ToLower().Contains("webm")) ||
                    (vextension.ToLower().Contains("m4v")) ||
                    (vextension.ToLower().Contains("mov")) ||
                    (vextension.ToLower().Contains("mp4")) ||
                    (vextension.ToLower().Contains("ogv"))
                    //(vextension.ToLower().Contains("wmv"))
                    )
                {
                    if (fileSize > 105381888)
                    {
                        var FileUploadPreviewResult = new
                        {
                            M4VURL = "#",
                            OGVURL = "#",
                            WEBMURL = "#",
                            M4V = false,
                            OGV = false,
                            WEBM = false,
                            IsError = true,
                            ErrorMessage = Utility.CompetitionErrorMessage.InvalidExtensionVideo
                        };


                        var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(FileUploadPreviewResult);

                        context.Response.Write(json);

                        return;
                    }
                    else
                    {

                        var resolvedFileName = System.IO.Path.GetFileName(thisV.FileName);

                        //fix for space, '#' and '&'  in filename
                        resolvedFileName = resolvedFileName.Trim().Replace(" ", "_");
                        resolvedFileName = resolvedFileName.Trim().Replace("#", "_");
                        resolvedFileName = resolvedFileName.Trim().Replace("&", "_");

                        //MCU-124 - multiple "." in uploaded filename
                        var splitCount = resolvedFileName.Split('.').Length;
                        if (splitCount > 2)
                        {
                            char replaceChar = '.';
                            int lastIndex = resolvedFileName.LastIndexOf(replaceChar);
                            if (lastIndex > -1)
                            {
                                System.Collections.Generic.IEnumerable<char> chars = resolvedFileName
                                    .Where((c, i) => c != replaceChar || i == lastIndex);
                                resolvedFileName = new string(chars.ToArray());
                            }
                        }

                        if (!System.IO.Directory.Exists(context.Server.MapPath("//temp_media")))
                            System.IO.Directory.CreateDirectory(context.Server.MapPath("//temp_media"));

                        var tempFile = System.String.Format("//temp_media//{0}", resolvedFileName);

                        /*delete existing temp files*/
                        DeleteMainFile(context, context.Server.MapPath(tempFile));
                        DeleteTempFiles(context, context.Server.MapPath(tempFile));
                        //if (System.IO.File.Exists(context.Server.MapPath(tempFile)))
                        //{
                        //    System.IO.File.Delete(context.Server.MapPath(tempFile));
                        //    string extensionLessFiles = tempFile.Replace('.', '_').Replace("//temp_media//", string.Empty);
                        //    foreach (string fi in System.IO.Directory.GetFiles(context.Server.MapPath("//temp_media"), extensionLessFiles + "*"))
                        //    {
                        //        if (System.IO.File.Exists(fi))
                        //            System.IO.File.Delete(fi);
                        //    }
                        //}

                        thisV.SaveAs(context.Server.MapPath(tempFile));
                        X.Media.Encoding.Encoder encoder = new X.Media.Encoding.Encoder();

                        if (encoder == null)
                        {
                            return;
                        }

                        //get video duration
                        System.TimeSpan duration = encoder.GetVideoTimeSpan(context.Server.MapPath(tempFile));

                        //MCU - 135
                        var maxDuration = new System.TimeSpan(0, 0, 60);

                        //less than 30 seconds check

                        //removed for 100MB video

                        if (duration > maxDuration)
                        {
                            //dispose encoder intance
                            encoder = null;
                            DeleteMainFile(context, context.Server.MapPath(tempFile));
                            DeleteTempFiles(context, context.Server.MapPath(tempFile));
                            var FileUploadPreviewResult = new
                            {
                                M4VURL = "#",
                                OGVURL = "#",
                                WEBMURL = "#",
                                M4V = false,
                                OGV = false,
                                WEBM = false,
                                IsError = true,
                                ErrorMessage = Utility.CompetitionErrorMessage.MaxDuration
                            };


                            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(FileUploadPreviewResult);

                            context.Response.Write(json);

                            return;
                        }

                        bool isM4VFileCreated = false;
                        bool isOGGFileCreated = false;
                        bool isWEBMFileCreated = false;
                        string outputFilePath = tempFile.Replace('.', '_');
                        try
                        {



                            isM4VFileCreated = encoder.EncodeVideo(context.Server.MapPath(tempFile), X.Media.Encoding.Format.Mp4, X.Media.Encoding.Quality.Custom450x336, context.Server.MapPath(outputFilePath + ".m4v"));
                            isOGGFileCreated = encoder.EncodeVideo(context.Server.MapPath(tempFile), X.Media.Encoding.Format.Ogg, X.Media.Encoding.Quality.Custom450x336, context.Server.MapPath(outputFilePath + ".ogv"));
                            isWEBMFileCreated = encoder.EncodeVideo(context.Server.MapPath(tempFile), X.Media.Encoding.Format.Webm, X.Media.Encoding.Quality.Custom450x336, context.Server.MapPath(outputFilePath + ".webm"));

                            if (!isM4VFileCreated && !isOGGFileCreated && !isOGGFileCreated)
                            {

                                //dispose encoder intance
                                encoder = null;
                                DeleteMainFile(context, context.Server.MapPath(tempFile));
                                DeleteTempFiles(context, context.Server.MapPath(tempFile));

                                var FileUploadPreviewResult = new
                                {
                                    M4VURL = "#",
                                    OGVURL = "#",
                                    WEBMURL = "#",
                                    M4V = false,
                                    OGV = false,
                                    WEBM = false,
                                    IsError = true,
                                    ErrorMessage = Utility.CompetitionErrorMessage.VideoPreviewError
                                };


                                var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(FileUploadPreviewResult);

                                context.Response.Write(json);

                                return;
                            }
                            else
                            {
                                var FileUploadPreviewResult = new
                                {
                                    M4VURL = VirtualPathUtility.ToAbsolute(outputFilePath + ".m4v"),
                                    OGVURL = VirtualPathUtility.ToAbsolute(outputFilePath + ".ogv"),
                                    WEBMURL = VirtualPathUtility.ToAbsolute(outputFilePath + ".webm"),
                                    M4V = isM4VFileCreated,
                                    OGV = isOGGFileCreated,
                                    WEBM = isWEBMFileCreated,
                                    IsError = false,
                                    ErrorMessage = ""
                                };


                                var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(FileUploadPreviewResult);

                                context.Response.Write(json);

                                return;
                            }

                        }
                        catch (System.Exception ex)
                        {
                            DeleteMainFile(context, context.Server.MapPath(tempFile));
                            DeleteTempFiles(context, context.Server.MapPath(tempFile));
                            var FileUploadPreviewResult = new
                            {
                                M4VURL = "#",
                                OGVURL = "#",
                                WEBMURL = "#",
                                M4V = false,
                                OGV = false,
                                WEBM = false,
                                IsError = true,
                                ErrorMessage = "Unexpected error occured on server. Please try again."
                            };


                            var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(FileUploadPreviewResult);

                            context.Response.Write(json);

                            return;
                        }
                    }
                }
                else
                {
                    var FileUploadPreviewResult = new
                    {
                        M4VURL = "#",
                        OGVURL = "#",
                        WEBMURL = "#",
                        M4V = false,
                        OGV = false,
                        WEBM = false,
                        IsError = true,
                        ErrorMessage = Utility.CompetitionErrorMessage.InvalidExtensionVideo
                    };
                    var json = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(FileUploadPreviewResult);

                    context.Response.Write(json);

                    return;
                }
            }
        }

        private void DeleteMainFile(HttpContext context, string path)
        {
            try
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
            catch (Exception ex)
            {

            }
        }

        private void DeleteTempFiles(HttpContext context, string path)
        {
            try
            {
                string extensionLessFiles = path.Replace('.', '_').Replace(context.Server.MapPath("//temp_media//"), string.Empty);
                foreach (string fi in System.IO.Directory.GetFiles(context.Server.MapPath("//temp_media"), extensionLessFiles + "*"))
                {
                    if (System.IO.File.Exists(fi))
                        System.IO.File.Delete(fi);
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void CleanUpOldVideos(HttpContext context)
        {
            try
            {
                if (Directory.Exists(context.Server.MapPath("//temp_media")))
                {
                    foreach (string fi in Directory.GetFiles(context.Server.MapPath("//temp_media")))
                    {
                        try
                        {
                            FileInfo file = new FileInfo(fi);
                            if (file.Exists && DateTime.UtcNow.Subtract(file.CreationTimeUtc) > new TimeSpan(0, context.Session.Timeout * 2, 0))
                            {
                                file.Delete();
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
    }
}