﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.media;
using umbraco.cms.businesslogic.web;
using Encoder = System.Text.Encoder;

namespace ImageHelper
{
    public class UmbracoMediaHelper
    {
        public static byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }

        public static System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }
        public static int MakeNewMedia(int parentId, string fileName, string mediaName)
        {
            var fname = Path.GetFileName(fileName);
            var media = Media.MakeNew(mediaName, MediaType.GetByAlias("Image"), new User(0), parentId);
            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/media") + @"\" + media.getProperty("umbracoFile").Id);
            string fullFilePath = HttpContext.Current.Server.MapPath("/media") + @"\" + media.getProperty("umbracoFile").Id + @"\" + fname;

            File.Move(HttpContext.Current.Server.MapPath(fileName), fullFilePath);

            FileStream fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

            Image image = System.Drawing.Image.FromStream(fs);

            var f = new FileInfo(fullFilePath);
            media.getProperty("umbracoFile").Value = "/media/" + media.getProperty("umbracoFile").Id + "/" + fname;
            media.getProperty("umbracoExtension").Value = f.Extension.Replace(".", "");
            media.getProperty("umbracoBytes").Value = f.Length.ToString();

            int fileWidth = image.Width;
            int fileHeight = image.Height;
            fs.Close();
            try
            {
                media.getProperty("umbracoWidth").Value = fileWidth.ToString();
                media.getProperty("umbracoHeight").Value = fileHeight.ToString();
            }
            catch { }

            var filename = fname.Split('.')[0];
            var fileext = fname.Split('.')[1];
            var thumbmotherpath = HttpContext.Current.Server.MapPath("/media/" + media.getProperty("umbracoFile").Id);
            string fileNameThumb = HttpContext.Current.Server.MapPath("/media/" + media.getProperty("umbracoFile").Id + "/" + filename + "_thumb.jpg");
            GenerateThumbnail(image, 100, fileWidth, fileHeight, thumbmotherpath, fileext, fileNameThumb);

            media.Save();

            return media.Id;
        }

        public static void GenerateThumbnail(Image image, int maxWidthHeight, int fileWidth, int fileHeight, string fullFilePath, string ext, string thumbnailFileName)
        {
            // Generate thumbnail
            float fx = (float)fileWidth / (float)maxWidthHeight;
            float fy = (float)fileHeight / (float)maxWidthHeight;
            // must fit in thumbnail size
            float f = Math.Max(fx, fy); //if (f < 1) f = 1;
            int widthTh = (int)Math.Round((float)fileWidth / f); int heightTh = (int)Math.Round((float)fileHeight / f);

            // fixes for empty width or height
            if (widthTh == 0)
                widthTh = 1;
            if (heightTh == 0)
                heightTh = 1;

            // Create new image with best quality settings
            Bitmap bp = new Bitmap(widthTh, heightTh);
            Graphics g = Graphics.FromImage(bp);
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            // Copy the old image to the new and resized
            Rectangle rect = new Rectangle(0, 0, widthTh, heightTh);
            g.DrawImage(image, rect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel);

            // Copy metadata
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            ImageCodecInfo codec = null;
            for (int i = 0; i < codecs.Length; i++)
            {
                if (codecs[i].MimeType.Equals("image/jpeg"))
                    codec = codecs[i];
            }

            // Set compresion ratio to 90%
            EncoderParameters ep = new EncoderParameters();
            ep.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L);

            // Save the new image
            bp.Save(thumbnailFileName, codec, ep);
            bp.Dispose();
            g.Dispose();

        }

        public static int MakeNewFile(int parentId, string fileName)
        {
            var fname = Path.GetFileName(fileName);
            var mediaName = fname;
            var media = Media.MakeNew(mediaName, MediaType.GetByAlias("File"), new User(0), parentId);
            Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/media") + @"\" + media.getProperty("umbracoFile").Id);
            string fullFilePath = HttpContext.Current.Server.MapPath("/media") + @"\" + media.getProperty("umbracoFile").Id + @"\" + fname;

            File.Move(HttpContext.Current.Server.MapPath(fileName), fullFilePath);

            var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

            var f = new FileInfo(fullFilePath);
            media.getProperty("umbracoFile").Value = "/media/" + media.getProperty("umbracoFile").Id + "/" + fname;
            media.getProperty("umbracoExtension").Value = f.Extension.Replace(".", "");
            media.getProperty("umbracoBytes").Value = f.Length.ToString();

            fs.Close();
            media.Save();

            return media.Id;
        }
    }
}
