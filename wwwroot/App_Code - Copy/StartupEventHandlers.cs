﻿using System.Linq;
using System.Web.Http;
using System.Web.Routing;
using System.Net.Http;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using umbraco.interfaces;
using wwwroot;
using umbraco.cms.businesslogic.web;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using umbraco.cms.businesslogic.member;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Xml.Serialization;
using umbraco;
using umbraco.BasePages;
using Umbraco.Core.Logging;


namespace Site.EventHandlers
{
    /// <summary>
    /// Class used to handle the Umbraco application events
    /// </summary>
    public class StartupEventHandlers : IApplicationStartupHandler
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StartupEventHandlers()
        {

            WebApiConfig.Register(GlobalConfiguration.Configuration);

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Umbraco.Web.UmbracoApplication.ApplicationStarting += UmbracoApplicationStarting;
            umbraco.macro.Error += macro_Error;


            //Register document event handlers
            Document.AfterPublish += Document_AfterPublish;
            Document.BeforePublish += Document_BeforePublish;
            Document.New += Document_New;


            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "From Mutti: StartupEventHandlers");
        }

        /// <summary>
        /// Event to handle validation before document published
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Document_BeforePublish(Document sender, umbraco.cms.businesslogic.PublishEventArgs e)
        {
            try
            {
                //When competition gets published, only one competition should be active.
                if (sender != null && sender.Template == 1083)
                {
                    // If user selects IsActive flag to true for a competition then for all other competitions, IsActive flag shold be set to false automatically.
                    if (sender.getProperty("isActive") != null && sender.getProperty("isActive").Value.ToString().Trim().Equals("1"))
                    {
                        var service = new ContentService();
                        var competitionList = (new umbraco.NodeFactory.Node(1088).ChildrenAsList).Where(x => x.template == 1083).ToList();

                        if (competitionList != null && competitionList.Count > 0)
                        {
                            foreach (var item in competitionList)
                            {
                                if (item.GetProperty("isActive") != null && item.Id != sender.Id)
                                {
                                    if (item.GetProperty("isActive").Value.ToString().Trim().Equals("1"))
                                    {
                                        var competitionNode = service.GetById(item.Id);
                                        competitionNode.SetValue("isActive", "0");
                                        service.SaveAndPublish(competitionNode);

                                        Utility.CompetitionHelper.setCompetitionHelperDefaults();
                                    }
                                }
                            }

                        }
                    }
                    else if (sender.getProperty("isActive") != null && sender.getProperty("isActive").Value.ToString().Trim().Equals("0"))
                    {
                        //If user selects IsActive flag to false for a competition then check that IsActive flag set to true for any other competition
                        //If IsActive flag is set to false for all other competitions, then don't allow to publish the competition and show error message.
                        var service = new ContentService();
                        var competitionList = (new umbraco.NodeFactory.Node(1088).ChildrenAsList).Where(x => x.template == 1083).ToList();

                        //Remove the current competition from list as we have to check that 
                        //all other comptitions are also inactive.
                        competitionList.RemoveAll(obj => obj.Id == sender.Id);

                        if (competitionList != null && competitionList.Count > 0)
                        {
                            bool isActive = competitionList.Exists(obj => obj.GetProperty("isActive").Value.ToString().Trim().Equals("1"));
                            if (!isActive)
                            {
                                e.Cancel = true;
                                umbraco.BasePages.BasePage.Current.ClientTools.CloseModalWindow();
                                umbraco.BasePages.BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=IsActive_false", "Validation Failed!", true, 300, 200, 100, 50, "", string.Empty);
                                umbraco.BasePages.BasePage.Current.ClientTools.ShowSpeechBubble(umbraco.BasePages.BasePage.speechBubbleIcon.error, "Publish Failed", "One comptition should be active!");
                            }
                        }
                    }
                }
                string steps = string.Empty;
                if (sender.ContentType.Alias.Equals("ProductItems", StringComparison.OrdinalIgnoreCase))
                {
                    steps = sender.GetProperty<string>("productItemDescription");
                }
                if (!string.IsNullOrEmpty(steps))
                {
                    string[] split = { "\r\n" };

                    var productItemDescriptionList = steps.Split(split, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var productItemDescriptionListItem in productItemDescriptionList)
                    {

                        if (productItemDescriptionListItem.Length > 100)
                        {
                            BasePage.Current.ClientTools.OpenModalWindow("/UmbracoCustomError?error=MaxLength", "Validation Failed!", true, 300, 130, 100, 50, "", string.Empty);
                            BasePage.Current.ClientTools.ShowSpeechBubble(BasePage.speechBubbleIcon.error, "Save Failed", "A description point can't contain more than 100 characters!");
                            e.Cancel = true;
                        }

                    }
                }
           
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Exeception occurend in Document_BeforePublish " + ex.StackTrace);
            }
        }

        /// <summary>
        /// Event to handle validation after document saved
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Document_AfterPublish(Document sender, umbraco.cms.businesslogic.PublishEventArgs e)
        {
            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "From Mutti: Document_AfterPublish");
            umbraco.library.UpdateDocumentCache(sender.Id);
        }

        /// <summary>
        /// Event to handle validation while creating new document
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Document_New(Document sender, umbraco.cms.businesslogic.NewEventArgs e)
        {
            if (sender != null)
            {
                if (sender.Template == 1083)
                {
                    //When author create new comptition node then all subnodes should be automatically created with all properties
                    var id = sender.Id;

                    var service = new ContentService();

                    var entry = service.CreateContent("Awaiting Approval", id, "entriesPage");
                    service.Save(entry);

                    entry = service.CreateContent("Entries", id, "entriesPage");
                    service.Save(entry);

                    entry = service.CreateContent("Denied Entries", id, "entriesPage");
                    service.Save(entry);

                    entry = service.CreateContent("Register", id, "competitionRegistration");
                    service.Save(entry);

                    entry = service.CreateContent("How to enter", id, "entryForm");
                    service.Save(entry);

                    entry = service.CreateContent("Enter", id, "submitAnEntry");
                    service.Save(entry);
                }


            }
        }

        void macro_Error(object sender, MacroErrorEventArgs e)
        {
            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "From Mutti: Macro Error " + e.Exception.StackTrace);
            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "From Mutti: Macro Error " + e.Name);
        }

        static void UmbracoApplicationStarting(object sender, System.EventArgs e)
        {

        }
    }
}