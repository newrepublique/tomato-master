﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FacebookConnect.ascx.cs" Inherits="UsercontrolsFacebookConnect" %>
<!-- this is the html shown when not logged in to FB -->
<asp:PlaceHolder runat="server" ID="FacebookLogin" Visible="false">
    <div id="fbcNotLoggedIn">
        <a class="fbcLogin" href="#" onclick="login();" title="Facebook Connect">Login to facebook</a>
        <script type="text/javascript">
            function login() {
                FB.Connect.requireSession();
                FB.Facebook.get_sessionState().waitUntilReady(function() { window.location = '<%=umbraco.library.NiceUrl(RedirectUrl) %>' });
                return false;
            }
        </script>
    </div>
</asp:PlaceHolder>

<!-- this is the html shown when the user is logged in to FB -->
<asp:PlaceHolder runat="server" ID="FacebookLogout" Visible="false">
    <div id="fbcLoggedIn">
        <div class="fbcPictureSquare"><asp:Image ID="imgFBPictureSquare" runat="server" /></div>
        <div class="fbcWelcome">Welcome <asp:Literal ID="LtlFBName" runat="server" /></div>
        <a class="fbcLogOut" href="#" onclick="FB.Connect.logoutAndRedirect(document.location.href)" title="Log out" >Logout</a>&nbsp;
        <a class="fbcShare" href="#" onclick="PublishContentToFB()" title="Publish Content" />Share on Facebook</a>
        <div class="clear">
        </div>
    </div>
</asp:PlaceHolder>



<!-- initscript for FB -->
<script type="text/javascript">
    FB.init("<asp:Literal ID="LtlFacebookAPIKey" runat="server" />");
    
    function PublishContentToFB() {
        FB.ensureInit(function() {
            var name = '<asp:Literal ID="FbShareTitle" runat="server" />';
            //var description = 'testDescription';
            var href = '<asp:Literal ID="FbShareUrl" runat="server" />';
            //var media = '';
            
            var attachment = {  'name': name,
                                'href': href,
                                //'description': description,
                                //'caption': 'This site is interesting'
                                //'media': media
            };
            
            FB.Connect.streamPublish('you have to read this', attachment, null, null, 'Show my contribution on Facebook', PublicContentToFacebook_callback);
        });
    }
    
    function PublicContentToFacebook_callback(post_id, exception) {
            alert('Content public: ' + post_id);
            
            //document.location.href = document.location.href + '&action=approve';
        }

</script>