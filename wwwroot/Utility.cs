﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using umbraco.interfaces;
using umbraco.presentation.nodeFactory;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;

namespace wwwroot
{
    public static class Utility
    {
        public static int CompetitionEntriesCount
        {
            get
            {
                int result = 12;

                string count = System.Configuration.ConfigurationManager.AppSettings["CompetitionEntriesCount"];

                if (!int.TryParse(count, out result))
                    result = 12;

                return result;

            }

        }


        public static void SetNoBrowserCacheForpage()
        {
            try
            {
                HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                HttpContext.Current.Response.Cache.SetExpires(DateTime.Now.AddSeconds(-1));
                HttpContext.Current.Response.Cache.SetNoStore();
                HttpContext.Current.Response.Cache.SetValidUntilExpires(false);
                HttpContext.Current.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);

                HttpContext.Current.Response.AppendHeader("Cache-control", "no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0");
                HttpContext.Current.Response.AppendHeader("pragma", "no-cache");
            }
            catch
            {
            }
        }

        public static bool UpdateEntryViewCount(int entryId)
        {
            var service = new ContentService();
            var entry = service.GetById(entryId);
            Node node = new Node(entryId);
            int competitionId = node.Parent.Parent.Id;

            var currentViews = 0;
            int.TryParse(entry.GetValue("entryViews").ToString(), out currentViews);

            entry.SetValue("entryViews", currentViews + 1);
            service.SaveAndPublish(entry);



            //refresh cache
            umbraco.library.UpdateDocumentCache(node.Parent.Id);
            umbraco.library.UpdateDocumentCache(entry.Id);
            umbraco.library.RefreshContent();

            return true;
        }

        public static class CompetitionErrorMessage
        {
            public static string InvalidExtensionVideo
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.InvalidExtensionVideo"];
                    return result == null ? string.Empty : result;
                }
            }

            public static string InvalidExtensionImage
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.InvalidExtensionImage"];
                    return result == null ? string.Empty : result;
                }
            }

            public static string MaxDuration
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.MaxDuration"];
                    return result == null ? string.Empty : result;
                }
            }

            public static string MaxFileSizeVideo
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.MaxFileSizeVideo"];
                    return result == null ? string.Empty : result;
                }
            }

            public static string MaxFileSizeImage
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.MaxFileSizeImage"];
                    return result == null ? string.Empty : result;
                }
            }

            public static string MaxTextWordLimit
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.MaxTextWordLimit"];
                    return result == null ? string.Empty : result;
                }
            }

            public static readonly string Generic = "We've encountered a problem... There's been an issue while uploading your submission, but all is not lost. Simply click the \"TRY AGAIN\" button below to retry.";

            public static string VideoPreviewError
            {
                get
                {
                    string result = System.Configuration.ConfigurationManager.AppSettings["Entry.VideoPreviewError"];
                    return result == null ? string.Empty : result;
                }
            }
        }

        public static class SessionHelper
        {
            public static List<INode> CurrentCompetitionEntries
            {
                get
                {
                    List<INode> result = null;

                    if (HttpContext.Current.Session["CurrentApprovedCompetitionEntries"] != null)
                        result = (List<INode>)HttpContext.Current.Session["CurrentApprovedCompetitionEntries"];

                    return result;

                }
                set
                {
                    HttpContext.Current.Session["CurrentApprovedCompetitionEntries"] = value;
                }
            }
        }

        /// <summary>
        /// Class for get Competition and it's Chield Node Ids
        /// </summary>
        public static class CompetitionHelper
        {
            /// <summary>
            /// Get Node Id for active competition
            /// </summary>
            private static int _activeCompetitionId = 1084;
            //private static int _activeCompetitionId;
            public static int ActiveCompetitionId
            {
                get
                {
                    if (_activeCompetitionId != 0)
                    {
                        return _activeCompetitionId;
                    }

                    var competitionList = (new umbraco.NodeFactory.Node(1088).ChildrenAsList).Where(x => x.template == 1083).ToList();
                    if (competitionList != null && competitionList.Count > 0)
                    {
                        var activeCompetition = competitionList.Where(x => x.GetProperty("isActive") != null && x.GetProperty("isActive").Value.ToString().Trim().Equals("1")).FirstOrDefault();

                        if (activeCompetition != null)
                        {
                            _activeCompetitionId = activeCompetition.Id;
                        }
                        else
                        {
                            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: No Active competition found");
                        }
                    }
                    else
                    {
                        LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: No competition found");
                    }
                    return _activeCompetitionId;
                }

                set
                {
                    _activeCompetitionId = value;
                }
            }

            /// <summary>
            /// Get Node Id for Register active competition
            /// </summary>
            private static int _registerPageNodeId;
            public static int RegisterPageNodeId
            {
                get
                {
                    if (_registerPageNodeId != 0)
                    {
                        return _registerPageNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;

                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;
                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.template == 1174).FirstOrDefault();
                                if (node != null)
                                    _registerPageNodeId = node.Id;
                            }
                        }
                    }
                    return _registerPageNodeId;
                }

                set
                {
                    _registerPageNodeId = value;
                }
            }

            /// <summary>
            /// Get Node Id for Enter for active competition
            /// </summary>
            private static int _enterNodeId;
            public static int EnterNodeId
            {
                get
                {
                    if (_enterNodeId != 0)
                    {
                        return _enterNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;
                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;

                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.template == 1206).FirstOrDefault();

                                if (node != null)
                                    _enterNodeId = node.Id;
                            }
                        }
                    }
                    return _enterNodeId;
                }

                set
                {
                    _enterNodeId = value;
                }
            }

            /// <summary>
            /// Get Node Id for How-To-Enter for active competition
            /// </summary>
            private static int _howToEnterNodeId;
            public static int HowToEnterNodeId
            {
                get
                {
                    if (_howToEnterNodeId != 0)
                    {
                        return _howToEnterNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;
                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;
                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.template == 1184).FirstOrDefault();
                                if (node != null)
                                    _howToEnterNodeId = node.Id;
                            }
                        }
                    }
                    return _howToEnterNodeId;
                }

                set
                {
                    _howToEnterNodeId = value;
                }
            }


            /// <summary>
            /// Get Node Id of Entry-Submitted-Successfully for the competition
            /// </summary>
            private static int _entrySubmittedSuccessfullyNodeId;
            public static int EntrySubmittedSuccessfullyNodeId
            {
                get
                {
                    if (_entrySubmittedSuccessfullyNodeId != 0)
                    {
                        return _entrySubmittedSuccessfullyNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;
                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;
                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.template == Convert.ToInt32(ConfigurationManager.AppSettings["entrySubmittedSuccessfullyNodeId"])).FirstOrDefault();
                                if (node != null)
                                    _entrySubmittedSuccessfullyNodeId = node.Id;
                            }
                        }
                    }
                    return _entrySubmittedSuccessfullyNodeId;
                }

                set
                {
                    _entrySubmittedSuccessfullyNodeId = value;
                }
            }





            /// <summary>
            /// Get Node Id for Entries for active competition
            /// </summary>
            private static int _entriesNodeId;
            public static int EntriesNodeId
            {
                get
                {
                    if (_entriesNodeId != 0)
                    {
                        return _entriesNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;
                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;
                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.Name.Trim().ToLower().Contains("entries")).FirstOrDefault();
                                if (node != null)
                                    _entriesNodeId = node.Id;
                            }
                        }
                    }
                    return _entriesNodeId;
                }

                set
                {
                    _entriesNodeId = value;
                }
            }

            /// <summary>
            /// Get Node Id for active competition
            /// </summary>
            private static int _awaitingApprovalEntriesNodeId;
            public static int AwaitingApprovalEntriesNodeId
            {
                get
                {
                    if (_awaitingApprovalEntriesNodeId != 0)
                    {
                        return _awaitingApprovalEntriesNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;
                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;
                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.Name.Trim().ToLower().Contains("awaiting")).FirstOrDefault();
                                if (node != null)
                                    _awaitingApprovalEntriesNodeId = node.Id;
                            }
                        }
                    }
                    return _awaitingApprovalEntriesNodeId;
                }

                set
                {
                    _awaitingApprovalEntriesNodeId = value;
                }
            }

            /// <summary>
            /// Get Node Id for Denied-Entries for active competition
            /// </summary>
            private static int _deniedEntriesNodeId;
            public static int DeniedEntriesNodeId
            {
                get
                {
                    if (_deniedEntriesNodeId != 0)
                    {
                        return _deniedEntriesNodeId;
                    }

                    var id = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;
                    if (id != 0)
                    {
                        var activeCompetition = new umbraco.NodeFactory.Node(id);
                        if (activeCompetition != null)
                        {
                            var childNodes = activeCompetition.ChildrenAsList;
                            if (childNodes != null && childNodes.Count > 0)
                            {
                                var node = childNodes.Where(x => x.Name.Trim().ToLower().Contains("denied")).FirstOrDefault();
                                if (node != null)
                                    _deniedEntriesNodeId = node.Id;
                            }
                        }
                    }
                    return _deniedEntriesNodeId;
                }

                set
                {
                    _deniedEntriesNodeId = value;
                }
            }

            /// <summary>
            /// Get Nice Url for active competition
            /// </summary>
            private static string _activeCompetitionNiceUrl;
            public static string ActiveCompetitionNiceUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_activeCompetitionNiceUrl))
                    {
                        return _activeCompetitionNiceUrl;
                    }
                    _activeCompetitionNiceUrl = new Node(wwwroot.Utility.CompetitionHelper.ActiveCompetitionId).NiceUrl;
                    return _activeCompetitionNiceUrl;
                }

                set
                {
                    _activeCompetitionNiceUrl = value;
                }
            }

            /// <summary>
            /// Get Nice Url for Entries for active competition
            /// </summary>
            private static string _entriesNodeNiceUrl;
            public static string EntriesNodeNiceUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_entriesNodeNiceUrl))
                    {
                        return _entriesNodeNiceUrl;
                    }
                    _entriesNodeNiceUrl = new Node(wwwroot.Utility.CompetitionHelper.EntriesNodeId).NiceUrl;
                    return _entriesNodeNiceUrl;
                }

                set
                {
                    _entriesNodeNiceUrl = value;
                }
            }

            /// <summary>
            /// Get Nice Url for Enter for active competition
            /// </summary>
            private static string _enterNodeNiceUrl;
            public static string EnterNodeNiceUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_enterNodeNiceUrl))
                    {
                        return _enterNodeNiceUrl;
                    }
                    _enterNodeNiceUrl = new Node(wwwroot.Utility.CompetitionHelper.EnterNodeId).NiceUrl;
                    return _enterNodeNiceUrl;
                }

                set
                {
                    _enterNodeNiceUrl = value;
                }
            }

            /// <summary>
            /// Get Nice Url for How-To-Enter for active competition
            /// </summary>
            private static string _howToEnterNodeNiceUrl;
            public static string HowToEnterNodeNiceUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_howToEnterNodeNiceUrl))
                    {
                        return _howToEnterNodeNiceUrl;
                    }
                    _howToEnterNodeNiceUrl = new Node(wwwroot.Utility.CompetitionHelper.HowToEnterNodeId).NiceUrl;
                    return _howToEnterNodeNiceUrl;
                }

                set
                {
                    _howToEnterNodeNiceUrl = value;
                }
            }


            /// <summary>
            /// Get Nice Url for Entry-Submitted-Successfully for competition
            /// </summary>
            private static string _entrySubmittedSuccessfullyNodeNiceUrl;
            public static string EntrySubmitedSuccessfullyNodeNiceUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_entrySubmittedSuccessfullyNodeNiceUrl))
                    {
                        return _entrySubmittedSuccessfullyNodeNiceUrl;
                    }
                    _entrySubmittedSuccessfullyNodeNiceUrl = new Node(wwwroot.Utility.CompetitionHelper.EntrySubmittedSuccessfullyNodeId).NiceUrl;
                    return _entrySubmittedSuccessfullyNodeNiceUrl;
                }

                set
                {
                    _entrySubmittedSuccessfullyNodeNiceUrl = value;
                }
            }


            /// <summary>
            /// Get Nice Url for Awaiting-Approval for active competition
            /// </summary>
            private static string _awaitingApprovalEntriesNodeNiceUrl;
            public static string AwaitingApprovalEntriesNodeNiceUrl
            {
                get
                {
                    if (!string.IsNullOrEmpty(_awaitingApprovalEntriesNodeNiceUrl))
                    {
                        return _awaitingApprovalEntriesNodeNiceUrl;
                    }
                    _awaitingApprovalEntriesNodeNiceUrl = new Node(wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId).NiceUrl;
                    return _awaitingApprovalEntriesNodeNiceUrl;
                }

                set
                {
                    _awaitingApprovalEntriesNodeNiceUrl = value;
                }
            }

            #region Methods
            /// <summary>
            /// Get Awaiting-Approval Entries Node Id By Competition Id
            /// </summary>
            /// <param name="competitionId"></param>
            /// <returns></returns>
            public static int getAwaitingApprovalEntriesNodeIdByCompetitionId(int competitionId)
            {
                int result = 0;
                if (competitionId != 0)
                {
                    var Competition = new umbraco.NodeFactory.Node(competitionId);
                    if (Competition != null)
                    {
                        var childNodes = Competition.ChildrenAsList;

                        if (childNodes != null && childNodes.Count > 0)
                        {
                            var node = childNodes.Where(x => x.Name.Trim().ToLower().Contains("awaiting")).FirstOrDefault();
                            if (node != null)
                                result = node.Id;
                        }
                    }
                }
                return result;
            }

            /// <summary>
            /// Get Denied-Entries Entries Node Id By Competition Id
            /// </summary>
            /// <param name="competitionId"></param>
            /// <returns></returns>
            public static int getDeniedEntriesNodeIdByCompetitionId(int competitionId)
            {
                int result = 0;
                if (competitionId != 0)
                {
                    var Competition = new umbraco.NodeFactory.Node(competitionId);
                    if (Competition != null)
                    {
                        var childNodes = Competition.ChildrenAsList;
                        if (childNodes != null && childNodes.Count > 0)
                        {
                            var node = childNodes.Where(x => x.Name.Trim().ToLower().Contains("denied")).FirstOrDefault();
                            if (node != null)
                                result = node.Id;
                        }
                    }
                }
                return result;
            }

            /// <summary>
            /// Get Entries Node Id By Competition Id
            /// </summary>
            /// <param name="competitionId"></param>
            /// <returns></returns>
            public static int getEntriesNodeIdByCompetitionId(int competitionId)
            {
                int result = 0;
                if (competitionId != 0)
                {
                    var Competition = new umbraco.NodeFactory.Node(competitionId);
                    if (Competition != null)
                    {
                        var childNodes = Competition.ChildrenAsList;

                        if (childNodes != null && childNodes.Count > 0)
                        {
                            var node = childNodes.Where(x => x.Name.Trim().ToLower().Contains("entries")).FirstOrDefault();
                            if (node != null)
                                result = node.Id;
                        }
                    }
                }
                return result;
            }

            /// <summary>
            /// Get Entries Node URL By Competition Id
            /// </summary>
            /// <param name="competitionId"></param>
            /// <returns></returns>
            public static string getEntriesNodeNiceURLByCompetitionId(int competitionId)
            {
                string result = string.Empty;
                if (competitionId != 0)
                {
                    var node = new umbraco.NodeFactory.Node(getEntriesNodeIdByCompetitionId(competitionId));
                    if (node != null)
                        result = node.NiceUrl;
                }
                return result;
            }

            /// <summary>
            /// Set the private properties to defaults
            /// </summary>
            public static void setCompetitionHelperDefaults()
            {
                ActiveCompetitionId = 0;
                RegisterPageNodeId = 0;
                EnterNodeId = 0;
                HowToEnterNodeId = 0;
                EntrySubmittedSuccessfullyNodeId = 0;
                EntriesNodeId = 0;
                AwaitingApprovalEntriesNodeId = 0;
                DeniedEntriesNodeId = 0;

                ActiveCompetitionNiceUrl = string.Empty;
                EntriesNodeNiceUrl = string.Empty;
                EnterNodeNiceUrl = string.Empty;
                HowToEnterNodeNiceUrl = string.Empty;
                AwaitingApprovalEntriesNodeNiceUrl = string.Empty;
            }
            #endregion
        }

    }
}