﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Facebook;
using Umbraco.Core.Logging;

namespace wwwroot.Controllers
{
    public class AuthController : Controller
    {
        public JsonResult Handshake(string access_token)
        {
            string me = string.Empty;

            try
            {
                var client = new FacebookClient(access_token);
                me = client.Get("me").ToString();
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: facebook handshake failed " + ex.StackTrace);
            }

            return Json(me, JsonRequestBehavior.AllowGet);
        }
    }
}
