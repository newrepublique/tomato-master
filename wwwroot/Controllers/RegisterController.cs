﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Services;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.member;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.web;
using wwwroot.Models;
using System.Net;
using System.Net.Http;
using ServiceStack.Text;
using System.IO;
using umbraco.cms.businesslogic.media;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using Umbraco.Core.Logging;
using umbraco.cms.businesslogic.propertytype;
using Umbraco.Core;
using umbraco;
using X.Media.Encoding;
using System.Configuration;
using System.Text.RegularExpressions;
//using ImageHelper;


namespace wwwroot.Controllers
{
    public class RegisterController : Controller
    {

        public JsonResult RegisterCompetition(ApplicantDetail applicant)
        {
            //Restict 2 different entries using the same email address
            bool isEntryExist = false;

            IEnumerable<Node> nodes = umbraco.uQuery.GetNodesByType("RecordVideoEntry");
            if (nodes != null && nodes.Count() > 0)
            {
                isEntryExist = nodes.ToList().Exists(entry => string.Equals(applicant.Email, entry.GetProperty<string>("email"), StringComparison.OrdinalIgnoreCase));
            }
            if (!isEntryExist)
            {
                Session["ApplicantSession"] = applicant;
                Session["isEntrySubmitted"] = false;
                return Json(Utility.CompetitionHelper.EnterNodeNiceUrl, JsonRequestBehavior.AllowGet);
            }

            return Json("InvalidEmail", JsonRequestBehavior.AllowGet);

        }


        private static int MakeNewFile(int parentId, string fileName)
        {
            var fname = Path.GetFileName(fileName);
            var mediaName = fname;
            var media = Media.MakeNew(mediaName, MediaType.GetByAlias("File"), new User(0), parentId);
            Directory.CreateDirectory(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "media") + @"\" + media.getProperty("umbracoFile").Id);
            string fullFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "media") + @"\" + media.getProperty("umbracoFile").Id + @"\" + fname;

            System.IO.File.Move(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName.Remove(0, 2)), fullFilePath);

            var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.Read);

            var f = new FileInfo(fullFilePath);
            media.getProperty("umbracoFile").Value = "/media/" + media.getProperty("umbracoFile").Id + "/" + fname;
            media.getProperty("umbracoExtension").Value = f.Extension.Replace(".", "");
            media.getProperty("umbracoBytes").Value = f.Length.ToString();

            fs.Close();
            media.Save();

            return media.Id;


        }

        public JsonResult SaveVideoFileEntry()
        {
            try
            {
                if (Request.Files != null)
                {
                    var thisV = Request.Files[0];
                    if (thisV != null)
                    {
                        var applicant = (ApplicantDetail)Session["ApplicantSession"];

                        var vextension = thisV.FileName.Split('.').ToList().Last();
                        var fileSize = thisV.ContentLength;
                        if (
                            (vextension.ToLower().Contains("webm")) ||
                            (vextension.ToLower().Contains("m4v")) ||
                            (vextension.ToLower().Contains("mov")) ||
                            (vextension.ToLower().Contains("mp4")) ||
                            (vextension.ToLower().Contains("ogv"))
                            //(vextension.ToLower().Contains("wmv"))
                            )
                        {
                            if (fileSize > 105381888)
                            {
                                return Json(Utility.CompetitionErrorMessage.InvalidExtensionVideo);
                            }
                            else
                            {

                                var ename = string.Format("{0} {1}", applicant.FirstName, applicant.LastName);
                                var resolvedFileName = Path.GetFileName(thisV.FileName);

                                //fix for space, '#' and '&'  in filename
                                resolvedFileName = resolvedFileName.Trim().Replace(" ", "_");
                                resolvedFileName = resolvedFileName.Trim().Replace("#", "_");
                                resolvedFileName = resolvedFileName.Trim().Replace("&", "_");

                                //MCU-124 - multiple "." in uploaded filename
                                var splitCount = resolvedFileName.Split('.').Count();
                                if (splitCount > 2)
                                {
                                    Char replaceChar = '.';
                                    int lastIndex = resolvedFileName.LastIndexOf(replaceChar);
                                    if (lastIndex != -1)
                                    {
                                        IEnumerable<Char> chars = resolvedFileName
                                            .Where((c, i) => c != replaceChar || i == lastIndex);
                                        resolvedFileName = new string(chars.ToArray());
                                    }
                                }

                                var tempFile = String.Format("//media//{0}", resolvedFileName);
                                thisV.SaveAs(Server.MapPath("~" + tempFile));
                                X.Media.Encoding.Encoder encoder = new X.Media.Encoding.Encoder();

                                if (encoder == null)
                                {
                                    LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: X.Media.Encoding.Encoder failed cannot transcode video , User - " + ename + ", Filename - " + resolvedFileName);

                                    return Json(Utility.CompetitionErrorMessage.MaxDuration);
                                }

                                //get video duration
                                TimeSpan duration = encoder.GetVideoTimeSpan(Server.MapPath("~" + tempFile));

                                //MCU - 135
                                var maxDuration = new TimeSpan(0, 0, 60);

                                //less than 30 seconds check

                                //removed for 100MB video

                                if (duration > maxDuration)
                                {
                                    encoder = null;
                                    return Json(Utility.CompetitionErrorMessage.MaxDuration);
                                }


                                //save file in media folder
                                //TODO: Not working

                                var mid = MakeNewFile(1276, tempFile);
                                Media m = new Media(mid);
                                m.Save();

                                //get umbraco created folder path
                                var inputFilePath = m.getProperty("umbracoFile").Value.ToString();
                                string outputFilePath = string.Empty;
                                if (!string.IsNullOrWhiteSpace(inputFilePath.Trim()))
                                {
                                    inputFilePath = inputFilePath.TrimStart('/');

                                    var splitResult = inputFilePath.Split('/');

                                    if (splitResult.Count() == 3)
                                    {
                                        //e.g. //media//12345
                                        inputFilePath = String.Format("//{0}//{1}", splitResult[0], splitResult[1]);

                                        //create required folder structure 
                                        var requiredLocation = "\\html5\\index.files\\html5video\\";
                                        inputFilePath = Server.MapPath("~" + inputFilePath);

                                        outputFilePath = inputFilePath + requiredLocation;
                                        Directory.CreateDirectory(outputFilePath);

                                        //update input and output file path
                                        inputFilePath = String.Concat(inputFilePath, "\\", resolvedFileName);
                                        outputFilePath = String.Concat(outputFilePath, resolvedFileName.Split('.').ToList().First());

                                        bool isM4VFileCreated = false;
                                        bool isOGGFileCreated = false;
                                        bool isWEBMFileCreated = false;

                                        try
                                        {
                                            isM4VFileCreated = encoder.EncodeVideo(inputFilePath, Format.Mp4, Quality.Custom450x336, outputFilePath + ".m4v");
                                            isOGGFileCreated = encoder.EncodeVideo(inputFilePath, Format.Ogg, Quality.Custom450x336, outputFilePath + ".ogv");
                                            isWEBMFileCreated = encoder.EncodeVideo(inputFilePath, Format.Webm, Quality.Custom450x336, outputFilePath + ".webm");
                                        }
                                        catch (Exception ex)
                                        {
                                            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error Transcoding video , User - " + ename + " Filename - " + resolvedFileName + ex.StackTrace);
                                        }

                                        if (!isM4VFileCreated && !isOGGFileCreated && !isOGGFileCreated)
                                        {
                                            encoder = null;
                                            return Json("Unexpected error occured on server. Please try again.");

                                        }

                                        bool isThumbnailGenerated = false;

                                        //extract thumbnail from trancoded video
                                        try
                                        {
                                            if (isM4VFileCreated)
                                            {
                                                if (System.IO.File.Exists(outputFilePath + ".m4v"))
                                                {
                                                    isThumbnailGenerated = encoder.GetThumbnail(outputFilePath + ".m4v", outputFilePath);
                                                }
                                            }
                                            else if (isOGGFileCreated)
                                            {
                                                if (System.IO.File.Exists(outputFilePath + ".ogv"))
                                                {
                                                    isThumbnailGenerated = encoder.GetThumbnail(outputFilePath + ".ogv", outputFilePath);
                                                }
                                            }
                                            else if (isWEBMFileCreated)
                                            {
                                                if (System.IO.File.Exists(outputFilePath + ".webm"))
                                                {
                                                    isThumbnailGenerated = encoder.GetThumbnail(outputFilePath + ".webm", outputFilePath);
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error extracting video thumbnail for , User - " + ename + " Filename - " + resolvedFileName + " " + ex.StackTrace);
                                        }
                                    }
                                    else
                                    {
                                        encoder = null;
                                        return Json("Unexpected error occured on server. Please try again.");
                                    }
                                }
                                else
                                {
                                    encoder = null;
                                    return Json("Unexpected error occured on server. Please try again.");


                                }

                                //dispose encoder intance
                                encoder = null;

                                var appName = Request["applicationName"];


                                SubmitEntry(appName, "0", "uploadvideo", "", Convert.ToString(mid));

                                return Json("sucess");
                            }
                        }
                        else
                        {
                            return Json(Utility.CompetitionErrorMessage.InvalidExtensionVideo);

                        }
                    }
                }
                return Json("Unexpected error occured on server. Please try again.");
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Upload Video file - " + ex.StackTrace);
                return Json("Unexpected error occured on server. Please try again.");
            }
        }


        public async Task<ActionResult> SubmitEntry(string entryName, string memberId, string entryType, string entryDescription, string entryUrl)
        {

            if (entryType == "uploadYoutubeVideo")
            {
                //append Id to youtube URL
                entryUrl = "http://www.youtube.com/watch?v=" + entryUrl;
            }
            //if (entryType == "uploadYoutubeVideo")
            //{
            //    string YoutubeVideoRegex = @"youtu(?:\.be|be\.com)/(?:.*v(?:/|=)|(?:.*/)?)([a-zA-Z0-9-_]+)";
            //    string url = entryUrl; //url goes here!

            //    Match youtubeMatch = Regex.Match(url, YoutubeVideoRegex);

            //    string id = string.Empty;

            //    if (youtubeMatch.Success)
            //    {
            //        id = youtubeMatch.Groups[1].Value;
            //    }
            //    if (!string.IsNullOrEmpty(id))
            //    {
            //        entryUrl = "http://www.youtube.com/watch?v=" + id;            
            //    }
            //    else
            //    {
            //        //TODO: Do something here
            //    }
            //}

            string successUrl = wwwroot.Utility.CompetitionHelper.EntrySubmitedSuccessfullyNodeNiceUrl;
            Redirect(successUrl);

            var entry = new EntryDetails
            {
                entryName = entryName,
                memberId = memberId,
                entryType = entryType,
                entryDescription = entryDescription,
                entryUrl = entryUrl,
            };

            var customer = Session["ApplicantSession"];

            if (customer != null)
            {
                var customerdetails = (ApplicantDetail)customer;

                Task.Run(() => IdentifyEntry(entry, customerdetails));
            }

            var obj = Json(new { Message = "Save", Value = successUrl });

            return obj;


        }

        private int IdentifyEntry(EntryDetails entryDetails, ApplicantDetail customerdetails)
        {
            int requiredId = 0;

            int result = 0;

            switch (entryDetails.entryType)
            {
                case "video":
                    requiredId = CreateVideoEntry(entryDetails, customerdetails);
                    break;
                case "write":
                    requiredId = CreateTextEntry(entryDetails, customerdetails);
                    break;
                case "image":
                    requiredId = CreateImageEntry(entryDetails, customerdetails);
                    break;
                case "uploadvideo":
                    requiredId = UploadVideoEntry(entryDetails, customerdetails);
                    break;
                case "uploadYoutubeVideo":
                    requiredId = UploadVideoEntry(entryDetails, customerdetails);
                    break;

            }

            if (requiredId != 0)
                result = requiredId;

            if (result > 0)
            {

                Node node = new Node(requiredId);

                if (node != null)
                {
                    //ExtractNodeSendEmail(node, 1270, EntryStatus.Create);
                }

            }

            return result;
        }

        public int UploadVideoEntry(EntryDetails entryDetails, ApplicantDetail customerdetails)
        {
            int requiredEntryId = 0;

            try
            {
                var service = new ContentService();
                var entry = service.CreateContent(entryDetails.entryName, wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId, "RecordVideoEntry");
                //Media file = new Media(int.Parse(entryDetails.entryUrl));
                //string url = file.getProperty("umbracoFile").Value.ToString();
                //string url = "http://www.youtube.com";

                string url = entryDetails.entryUrl;
                if (entryDetails.entryType == "uploadYoutubeVideo" && url.Contains("youtube"))
                {
                    url = entryDetails.entryUrl;
                }
                else if (entryDetails.entryType == "uploadvideo")
                {
                    Media file = new Media(int.Parse(entryDetails.entryUrl));
                    url = file.getProperty("umbracoFile").Value.ToString();
                }


                entry.SetValue("entryName", entryDetails.entryName);
                entry.SetValue("member", entryDetails.memberId);
                entry.SetValue("entryType", "video");
                entry.SetValue("entryDescription", entryDetails.entryDescription);
                entry.SetValue("entryUrl", url);
                entry.SetValue("dateSaved", DateTime.Now.ToShortDateString());
                entry.SetValue("entryShare", "0");
                entry.SetValue("entryLikes", "0");
                entry.SetValue("entryViews", "0");

                if (customerdetails != null)
                {
                    //entry.SetValue("productCode", customerdetails.ProductCode);
                    entry.SetValue("firstName", customerdetails.FirstName);
                    entry.SetValue("lastName", customerdetails.LastName);
                    entry.SetValue("email", customerdetails.Email);
                    entry.SetValue("phone", customerdetails.Phone);
                    entry.SetValue("postCode", customerdetails.PostCode);
                    entry.SetValue("state", customerdetails.State);

                    service.Save(entry);

                    entry.Name = String.Format("{0} - {1}", entry.Name, entry.Id);

                    service.SaveAndPublish(entry);

                    requiredEntryId = entry.Id;

                }
                else
                    return requiredEntryId;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Upload Video Entry - " + entryDetails.entryName + " Publish Failed. " + ex.StackTrace);
                return requiredEntryId;
            }
            finally
            {
                if (requiredEntryId != 0)
                    umbraco.library.UpdateDocumentCache(requiredEntryId);

                umbraco.library.UpdateDocumentCache(wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId);
                //umbraco.library.RefreshContent();

            }
            return requiredEntryId;
        }


        [HttpPost]
        public JsonResult UpdateEntryName(string entryName, string entryId)
        {
            var service = new ContentService();
            int entry_id = Convert.ToInt32(entryId);
            string EntryNiceUrl = string.Empty;
            try
            {
                if (entry_id > 0)
                {
                    var EntryNodeValues = service.GetById(entry_id);
                    EntryNodeValues.SetValue("entryName", entryName);
                    service.SaveAndPublish(EntryNodeValues);
                    Node EntryNode = new Node(entry_id);
                    EntryNiceUrl = EntryNode.NiceUrl;




                }
            }
            finally
            {
                umbraco.library.RefreshContent();
                if (entry_id != 0)
                    umbraco.library.UpdateDocumentCache(entry_id);

                umbraco.library.UpdateDocumentCache(wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId);
                umbraco.library.RefreshContent();

            }
            return Json(EntryNiceUrl);
        }



        public int CreateVideoEntry(EntryDetails entryDetails, ApplicantDetail customerdetails)
        {
            int requiredEntryId = 0;

            try
            {
                var service = new ContentService();
                var entry = service.CreateContent(entryDetails.entryName, wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId, "RecordVideoEntry");

                entry.SetValue("entryName", entryDetails.entryName);
                entry.SetValue("member", entryDetails.memberId);
                entry.SetValue("entryType", entryDetails.entryType);
                entry.SetValue("entryDescription", entryDetails.entryDescription);
                entry.SetValue("entryUrl", entryDetails.entryUrl);
                entry.SetValue("dateSaved", DateTime.Now.ToShortDateString());
                entry.SetValue("entryShare", "0");
                entry.SetValue("entryLikes", "0");
                entry.SetValue("entryViews", "0");

                if (customerdetails != null)
                {
                    entry.SetValue("firstName", customerdetails.FirstName);
                    entry.SetValue("lastName", customerdetails.LastName);
                    entry.SetValue("email", customerdetails.Email);
                    entry.SetValue("phone", customerdetails.Phone);
                    entry.SetValue("postCode", customerdetails.PostCode);
                    entry.SetValue("state", customerdetails.State);

                    service.Save(entry);

                    entry.Name = String.Format("{0} - {1}", entry.Name, entry.Id);

                    service.SaveAndPublish(entry);

                    requiredEntryId = entry.Id;

                }
                else
                    return requiredEntryId;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Create Video Entry - " + entryDetails.entryName + " Publish Failed. " + ex.StackTrace);
                return requiredEntryId;
            }
            finally
            {
                if (requiredEntryId != 0)
                    umbraco.library.UpdateDocumentCache(requiredEntryId);

                umbraco.library.UpdateDocumentCache(wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId);
                umbraco.library.RefreshContent();

            }
            return requiredEntryId;
        }


        //Update Entry Name and get Url For that entry




        public int CreateTextEntry(EntryDetails entryDetails, ApplicantDetail customerdetails)
        {
            int requiredEntryId = 0;

            try
            {
                if (!string.IsNullOrEmpty(entryDetails.entryUrl))
                {
                    //MCU-127 - special characters in text entry
                    entryDetails.entryUrl = entryDetails.entryUrl.Replace("&amp;", "&");
                    entryDetails.entryUrl = entryDetails.entryUrl.Replace("&gt;", ">");
                    entryDetails.entryUrl = entryDetails.entryUrl.Replace("&lt;", "<");
                }

                var service = new ContentService();
                var entry = service.CreateContent(entryDetails.entryName, wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId, "WriteTextEntry");

                entry.SetValue("entryName", entryDetails.entryName);
                entry.SetValue("member", entryDetails.memberId);
                entry.SetValue("entryType", entryDetails.entryType);
                entry.SetValue("entryDescription", entryDetails.entryDescription);
                entry.SetValue("entryUrl", entryDetails.entryUrl);
                entry.SetValue("dateSaved", DateTime.Now.ToShortDateString());
                entry.SetValue("entryShare", "0");
                entry.SetValue("entryLikes", "0");
                entry.SetValue("entryViews", "0");

                if (customerdetails != null)
                {
                    //entry.SetValue("productCode", customerdetails.ProductCode);
                    entry.SetValue("firstName", customerdetails.FirstName);
                    entry.SetValue("lastName", customerdetails.LastName);
                    entry.SetValue("email", customerdetails.Email);
                    // entry.SetValue("address1", customerdetails.Address1);
                    //entry.SetValue("address2", customerdetails.Address2);
                    entry.SetValue("state", customerdetails.State);

                    service.Save(entry);

                    entry.Name = String.Format("{0} - {1}", entry.Name, entry.Id);

                    service.SaveAndPublish(entry);

                    requiredEntryId = entry.Id;
                }
                else
                    return requiredEntryId;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Create Text Entry - " + entryDetails.entryName + " Publish Failed. " + ex.StackTrace);
                return requiredEntryId;
            }
            finally
            {
                if (requiredEntryId != 0)
                    umbraco.library.UpdateDocumentCache(requiredEntryId);

                umbraco.library.UpdateDocumentCache(wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId);
                umbraco.library.RefreshContent();
            }

            return requiredEntryId;
        }

        public int CreateImageEntry(EntryDetails entryDetails, ApplicantDetail customerdetails)
        {
            int requiredEntryId = 0;

            try
            {
                var service = new ContentService();
                var entry = service.CreateContent(entryDetails.entryName, wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId, "UploadImageEntry");

                entry.SetValue("entryName", entryDetails.entryName);
                entry.SetValue("member", entryDetails.memberId);
                entry.SetValue("entryType", entryDetails.entryType);
                entry.SetValue("entryDescription", entryDetails.entryDescription);
                entry.SetValue("entryUrl", entryDetails.entryUrl);
                entry.SetValue("dateSaved", DateTime.Now.ToShortDateString());
                entry.SetValue("entryShare", "0");
                entry.SetValue("entryLikes", "0");
                entry.SetValue("entryViews", "0");
                if (customerdetails != null)
                {

                    //entry.SetValue("productCode", customerdetails.ProductCode);
                    entry.SetValue("firstName", customerdetails.FirstName);
                    entry.SetValue("lastName", customerdetails.LastName);
                    entry.SetValue("email", customerdetails.Email);
                    //  entry.SetValue("address1", customerdetails.Address1);
                    //  entry.SetValue("address2", customerdetails.Address2);
                    entry.SetValue("state", customerdetails.State);

                    service.Save(entry);

                    entry.Name = String.Format("{0} - {1}", entry.Name, entry.Id);

                    service.SaveAndPublish(entry);

                    requiredEntryId = entry.Id;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Create Image Entry - " + entryDetails.entryName + " Publish Failed. " + ex.StackTrace);
                return requiredEntryId;
            }
            finally
            {
                if (requiredEntryId != 0)
                    umbraco.library.UpdateDocumentCache(requiredEntryId);

                umbraco.library.UpdateDocumentCache(wwwroot.Utility.CompetitionHelper.AwaitingApprovalEntriesNodeId);
                umbraco.library.RefreshContent();

            }
            return requiredEntryId;
        }

        public bool UpdateEntry(int entryId, string commandtype)
        {
            var service = new ContentService();
            var entry = service.GetById(entryId);
            Node node = new Node(entryId);
            int competitionId = node.Parent.Parent.Id;
            switch (commandtype)
            {
                case "Approve":
                    int entriesNodeId = wwwroot.Utility.CompetitionHelper.getEntriesNodeIdByCompetitionId(competitionId);
                    service.Move(entry, entriesNodeId);
                    entry.SetValue("parentNode", entriesNodeId);
                    service.SaveAndPublish(entry);

                    ExtractNodeSendEmail(node, 1271, EntryStatus.Approve);

                    break;
                case "Views":
                    var currentViews = 0;
                    int.TryParse(entry.GetValue("entryViews").ToString(), out currentViews);

                    entry.SetValue("entryViews", currentViews + 1);
                    service.SaveAndPublish(entry);
                    break;
                case "Share":
                    var currentShares = 0;
                    int.TryParse(entry.GetValue("entryShare").ToString(), out currentShares);

                    entry.SetValue("entryShare", currentShares + 1);
                    service.SaveAndPublish(entry);
                    break;
                case "Likes":
                    var currentLikes = 0;
                    int.TryParse(entry.GetValue("entryLikes").ToString(), out currentLikes);

                    entry.SetValue("entryLikes", currentLikes + 1);
                    service.SaveAndPublish(entry);
                    break;
            }

            //refresh cache
            umbraco.library.UpdateDocumentCache(node.Parent.Id);
            umbraco.library.UpdateDocumentCache(entry.Id);
            umbraco.library.RefreshContent();

            return true;
        }

        public bool UpdateEntry1(int entryId, string commandtype, string denyreason)
        {
            var service = new ContentService();
            var entry = service.GetById(entryId);
            Node node = new Node(entryId);
            int competitionId = node.Parent.Parent.Id;
            try
            {

                if (commandtype == "Deny")
                {
                    int deniedEntriesNodeId = wwwroot.Utility.CompetitionHelper.getDeniedEntriesNodeIdByCompetitionId(competitionId);
                    service.Move(entry, deniedEntriesNodeId);
                    entry.SetValue("parentNode", deniedEntriesNodeId);
                    entry.SetValue("denyReason", denyreason);
                    service.SaveAndPublish(entry);

                    //ExtractNodeSendEmail(node, 1272, EntryStatus.Deny, denyreason);

                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Denied Entry Failed Entry Id - " + entryId + " " + ex.StackTrace);
            }
            finally
            {
                umbraco.library.UpdateDocumentCache(wwwroot.Utility.CompetitionHelper.getDeniedEntriesNodeIdByCompetitionId(competitionId));
                umbraco.library.UpdateDocumentCache(entryId);
                umbraco.library.RefreshContent();
            }

            return true;

        }

        /// <summary>
        /// Mark entry as winning entry for respective competition
        /// </summary>
        /// <param name="entryId"></param>
        /// <returns></returns>
        public bool MarkAsWinningEntry(int entryId)
        {
            try
            {
                //Get competition node for entry
                var service = new ContentService();
                Node entryNode = new Node(entryId);
                int competitionNodeId = entryNode.Parent.Parent.Id;
                var competition = service.GetById(competitionNodeId);

                //Save Winning Entry node Id to competition node
                competition.SetValue("winningEntryId", entryId);
                service.SaveAndPublish(competition);

                //refresh cache
                umbraco.library.UpdateDocumentCache(competitionNodeId);
                umbraco.library.RefreshContent();
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mark As Winning Entry Failed " + ex.StackTrace);
            }
            return false;

        }


        /* Controller for saving the 3 Finalists */
        public bool MarkAsFinalistEntry(int entryId1, int entryId2, int entryId3)
        {
            try
            {
                //Get competition node for entry                        
                var service = new ContentService();

                int competitionNodeId = wwwroot.Utility.CompetitionHelper.ActiveCompetitionId;

                var competition = service.GetById(competitionNodeId);

                //Save Finalist Entry node Id to competition node
                competition.SetValue("finalist1EntryId", entryId1);
                competition.SetValue("finalist2EntryId", entryId2);
                competition.SetValue("finalist3EntryId", entryId3);

                service.SaveAndPublish(competition);
                umbraco.library.UpdateDocumentCache(competitionNodeId);
                umbraco.library.RefreshContent();
                return true;
            }

            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mark As Finalist Entry Failed " + ex.StackTrace);

            }
            return false;
        }

        private static void Sendmail(bool smtpSSLConvertBool, string smtpServer, int smtpPort, string smtpUser, string smtpPass, string smtpMailFrom, string smtpMailTo, string subjectTitle, string bodyMessage, Attachment attachment = null)
        {
            try
            {
                var smtp = new SmtpClient(smtpServer, smtpPort)
                {
                    EnableSsl = smtpSSLConvertBool,
                    Credentials = new NetworkCredential(smtpUser, smtpPass)
                };

                using (var message = new MailMessage(smtpMailFrom, smtpMailTo)
                {
                    Subject = subjectTitle,
                    Body = bodyMessage,
                    IsBodyHtml = true
                })
                {
                    if (attachment != null)
                    {
                        message.Attachments.Add(attachment);
                    }
                    smtp.Send(message);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Sent Email Failed " + ex.StackTrace);
            }
        }

        /// <summary>
        /// Extact Specific node details and sends email for create, approve and deny
        /// </summary>
        /// <param name="node">specific umbraco node</param>
        /// <param name="documentId">specific umbraco email template id</param>
        /// <param name="status">status - Create, Approve or Deny</param>
        /// <param name="message">optional message to be sent with email</param>
        private void ExtractNodeSendEmail(Node node, int documentId, EntryStatus status, string message = "")
        {
            try
            {
                if (node != null)
                {
                    #region extract node information and create email message

                    var name = node.GetProperty("firstName").Value; //+ " " + node.GetProperty("lastName").Value;
                    var email = node.GetProperty("email").Value;

                    string myUrl = "http://" + node.GetFullNiceUrl().ToLower().Replace("awaiting-approval", "entries");
                    var bodyContent = "";
                    var doc = new Node(documentId);

                    switch (status)
                    {
                        case EntryStatus.Approve:
                            var id = "";

                            switch (node.NodeTypeAlias)
                            {
                                case "RecordVideoEntry":
                                    id = "content-box2_" + node.Id;
                                    break;
                                case "UploadImageEntry":
                                    id = "content-box_" + node.Id;
                                    break;
                                case "WriteTextEntry":
                                    id = "content-box3_" + node.Id;
                                    break;
                            }

                            //if (HttpContext.Request.Url != null)
                            //    message = "http://" + HttpContext.Request.Url.Authority + Utility.CompetitionHelper.getEntriesNodeNiceURLByCompetitionId(node.Parent.Parent.Id) + "#" + id;

                            //bodyContent = doc.GetProperty("emailBodyContent").Value.ToString()
                            //                       .Replace("[entrylink]", message);

                            bodyContent = doc.GetProperty("emailBodyContent").Value.ToString().Replace("{{NAME}}", name).Replace("{{ENTRY_URL}}", myUrl);
                            break;
                        //case EntryStatus.Deny:

                        //    bodyContent = doc.GetProperty("emailBodyContent").Value.ToString()
                        //           .Replace("[reason]", message);

                        //    break;
                        //case EntryStatus.Create:
                        //    bodyContent = doc.GetProperty("emailBodyContent").Value.ToString().Replace("[name]", name).Replace("[entryUrl]", myUrl);
                        //    break;
                        default:
                            break;
                    }

                    #region assign href

                    if (!string.IsNullOrWhiteSpace(bodyContent))
                        bodyContent = bodyContent.Replace("href=\"/", "href=\"" + "http:////" + HttpContext.Request.Url.Authority + "/");

                    #endregion


                    var emailSettingsPicker = doc.GetProperty("emailSettingsPicker").Value.ToString();
                    var emailSettings = new Node(int.Parse(emailSettingsPicker));
                    var smtpServer = (string)emailSettings.GetProperty("mailServer").Value;
                    var smtpPort = int.Parse(emailSettings.GetProperty("mailPort").Value.ToString());
                    var smtpMailFrom = (string)emailSettings.GetProperty("emailFrom").Value;
                    var smtpUser = (string)emailSettings.GetProperty("mailUser").Value;
                    var smtpPass = (string)emailSettings.GetProperty("mailPassword").Value;
                    var smtpSSL = emailSettings.GetProperty("isSSL").Value.ToString();

                    bool smtpSSLConvertBool = smtpSSL.Trim().Equals("1") ? true : false;

                    var subjectTitle = doc.GetProperty("subjectContent").Value.ToString().Replace("[name]", name);

                    #endregion

                    //send email
                    Sendmail(smtpSSLConvertBool, smtpServer, smtpPort, smtpUser, smtpPass, smtpMailFrom, email, subjectTitle, bodyContent, null);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Email Sending Failed " + ex.StackTrace);
            }
        }

        private enum EntryStatus
        {
            Approve,
            Deny,
            Create
        }
    }
}
