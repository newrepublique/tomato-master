﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ServiceStack.Text;
using umbraco.NodeFactory;

namespace wwwroot.Controllers
{
    public class RecipeController : ApiController
    {
        // GET api/<controller>
        public async Task<HttpResponseMessage> Get()
        {
            var dishesList = new Node(1080);//DISHES LIST
            var selectedDishes = await Task.Run(()=> dishesList.ChildrenAsList);

            var obj = this.Request.CreateResponse(
                    HttpStatusCode.OK,
                    new { Message = "Hello", Value = 123 });

            return obj;
        }

        // GET api/<controller>/5
        public async Task<HttpResponseMessage> Get(int id)
        {
            var dishesTypeList = new Node(id);
            var dishesList = new Node(1080);//DISHES LIST
            var selectedDishes = await Task.Run(()=>dishesList.ChildrenAsList);

            var obj = this.Request.CreateResponse(
                    HttpStatusCode.OK,
                    new { Message = "Hello", Value = 123 });

            return obj;
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}