﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using umbraco.cms.businesslogic.web;
using umbraco.presentation.nodeFactory;

namespace wwwroot.Controllers
{
    public class SendMailController : Controller
    {
        //
        // GET: /SendMail/
        public async Task<JsonResult> SubmitMail(string Name, string Email)
        {
            var name = Name;
            var email = Email;

            var doc = new Document(1271);

            var emailSettingsPicker = doc.getProperty("emailSettingsPicker").Value.ToString();
            var emailSettings = new Document(int.Parse(emailSettingsPicker));
            var smtpServer = (string) emailSettings.getProperty("mailServer").Value;
            var smtpPort = int.Parse(emailSettings.getProperty("mailPort").Value.ToString());
            var smtpMailFrom = (string) emailSettings.getProperty("emailFrom").Value;
            var smtpUser = (string) emailSettings.getProperty("mailUser").Value;
            var smtpPass = (string) emailSettings.getProperty("mailPassword").Value;

            var smtpSSL = emailSettings.getProperty("isSSL").Value.ToString();
            bool smtpSSLConvertBool = false;

            if (smtpSSL == "1")
            {
                smtpSSL = "true";
                smtpSSLConvertBool = Boolean.Parse(smtpSSL);
            }
            if (smtpSSL == "0")
            {
                smtpSSL = "false";
                smtpSSLConvertBool = Boolean.Parse(smtpSSL);
            }


            var bodyContent = "";
            var subject = "";

            bodyContent = emailSettings.getProperty("emailBodyContent").Value.ToString().Replace("[name]", name);
            subject = emailSettings.getProperty("subject").Value.ToString();

            await
                Task.Run(
                    () =>
                    Sendmail(smtpSSLConvertBool, smtpServer, smtpPort, smtpUser, smtpPass, smtpMailFrom, email, subject,
                             bodyContent, null));
            return Json(new {Msg = "Success"});
        }

        private void Sendmail(bool smtpSSLConvertBool, string smtpServer, int smtpPort, string smtpUser, string smtpPass,
                              string smtpMailFrom, string smtpMailTo, string subjectTitle, string bodyMessage,
                              Attachment attachment = null)
        {
            var smtp = new SmtpClient(smtpServer, smtpPort)
                           {
                               EnableSsl = smtpSSLConvertBool,
                               Credentials = new NetworkCredential(smtpUser, smtpPass)
                           };
            using (var message = new MailMessage(smtpMailFrom, smtpMailTo)
                                     {
                                         Subject = subjectTitle,
                                         Body = bodyMessage,
                                         IsBodyHtml = true
                                     })
            {
                if (attachment != null)
                {
                    message.Attachments.Add(attachment);
                }
                smtp.Send(message);
            }
        }

       /** public bool dataEntry(int Id)
        {
            Node currentId = new Node(Id);
            foreach (Node childId in currentId.Children)
            {
                var Name = childId.GetProperty("entryName").Value;
                var date = String.Format("{0:M/d/yyyy}", childId.CreateDate) + "at";
                var time = childId.CreateDate.ToString("hh:mm tt");
            }
            return true;
        }**/
    }
}
