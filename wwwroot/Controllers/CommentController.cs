﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Umbraco.Core.Services;
using umbraco.BusinessLogic;
using umbraco.cms.businesslogic.member;
using umbraco.NodeFactory;
using umbraco.cms.businesslogic.web;
using wwwroot.Models;
using System.Net;
using System.Net.Http;
using ServiceStack.Text;
using System.IO;
using umbraco.cms.businesslogic.media;
using Umbraco.Core.Logging;

namespace wwwroot.Controllers
{
    public class CommentController : Controller
    {
        public class RegisterForCommentModel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Email { get; set; }
            public string Picture { get; set; }
        }

        public async Task<JsonResult> AddComment(Comment comment)
        {
            var resultdata = await Task.Run(() => AddingComment(comment));
            return Json(resultdata, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> GetComment(int status, int competitionNodeId = 0)
        {
            var resultdata = await Task.Run(() => GettingComment(status, competitionNodeId));
            return Json(resultdata, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ApproveComment(Comment comment)
        {
            var resultdata = await Task.Run(() => ApprovedComment(comment));
            return Json(resultdata, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DenyComment(Comment comment)
        {
            var resultdata = await Task.Run(() => DenyingComment(comment));
            return Json(resultdata, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> RegForComment(RegisterForCommentModel comment)
        {
            var resultdata = await Task.Run(() => RegisterForComment(comment));
            return Json(resultdata, JsonRequestBehavior.AllowGet);
        }

        private bool RegisterForComment(RegisterForCommentModel comment)
        {
            Session["RegisterForCommentModel"] = comment;
            return true;
        }

        private bool AddingComment(Comment comment)
        {
            bool result = false;

            try
            {
                if (Session["RegisterForCommentModel"] != null)
                {
                    var customerdetail = (RegisterForCommentModel)Session["RegisterForCommentModel"];

                    using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                    {

                        if (customerdetail.Picture == null)
                            customerdetail.Picture = "/images/avatar-1.jpg";

                        var query = @"INSERT INTO EntryComment (Comment, FirstName, LastName, Email, DateCreated, EntryId, EntriesUrl, UserPicture, Status)
                        VALUES (@Comment, @FirstName, @LastName, @Email, @DateCreated, @EntryId, @EntriesUrl, @UserPicture, 0)";

                        sqlHelper.ExecuteNonQuery(query,
                        sqlHelper.CreateParameter("@Comment", comment.CommentDescription),
                        sqlHelper.CreateParameter("@FirstName", customerdetail.FirstName),
                        sqlHelper.CreateParameter("@LastName", customerdetail.LastName),
                        sqlHelper.CreateParameter("@Email", customerdetail.Email),
                        sqlHelper.CreateParameter("@DateCreated", DateTime.Now),
                        sqlHelper.CreateParameter("@EntryId", comment.EntryId),
                        sqlHelper.CreateParameter("@EntriesUrl", comment.EntriesUrl),
                        sqlHelper.CreateParameter("@UserPicture", customerdetail.Picture));

                        result = true;
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error while adding comment " + ex.StackTrace);
                result = false;
            }

            return result;
        }

        private bool DenyingComment(Comment comment)
        {
            bool result = false;

            try
            {
                using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                {

                    var query = @"UPDATE EntryComment SET Status=@Status WHERE PkCommentId = @PkCommentId";

                    sqlHelper.ExecuteScalar<int>(query,
                    sqlHelper.CreateParameter("@Status", 2),
                    sqlHelper.CreateParameter("@PkCommentId", comment.PkCommentId));

                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error while denying comment " + ex.StackTrace);
                result = false;
            }

            return result;
        }

        private bool ApprovedComment(Comment comment)
        {
            bool result = false;

            try
            {
                using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                {

                    var query = @"UPDATE EntryComment SET Status=@Status WHERE PkCommentId = @PkCommentId";

                    sqlHelper.ExecuteScalar<int>(query,
                    sqlHelper.CreateParameter("@Status", 1),
                    sqlHelper.CreateParameter("@PkCommentId", comment.PkCommentId));

                    result = true;
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error while denying comment " + ex.StackTrace);
                result = false;
            }

            return result;
        }

        public JsonResult FetchComment(int entryId)
        {
            string result = string.Empty;
            List<Comment> comments = new List<Comment>();

            try
            {
                using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                {
                    var query = @"SELECT * FROM EntryComment WHERE Status = @Status and EntryId = @EntryId";

                    var reader = sqlHelper.ExecuteReader(query,
                    sqlHelper.CreateParameter("@Status", 1),
                    sqlHelper.CreateParameter("@EntryId", entryId));

                    if (reader != null)
                    {
                        while (reader.Read())
                        {
                            comments.Add(new Comment
                            {
                                PkCommentId = reader.GetInt("PkCommentId"),
                                EntryId = reader.GetInt("EntryId"),
                                FirstName = reader.GetString("FirstName"),
                                LastName = reader.GetString("LastName"),
                                CommentDescription = reader.GetString("Comment"),
                                DateCreated = reader.GetDateTime("DateCreated"),
                                Email = reader.GetString("Email"),
                                Status = reader.GetInt("Status"),
                                EntriesUrl = reader.GetString("EntriesUrl"),
                                UserPicture = reader.GetString("UserPicture")
                            });
                        }

                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error while fetching comment for entryid " + entryId+ " " + ex.StackTrace);
            }

            return Json(comments, JsonRequestBehavior.AllowGet);
        }


        public List<Comment> GettingComment(int status, int competitionNodeId)
        {
            List<Comment> comments = new List<Comment>();

            try
            {
                using (var sqlHelper = umbraco.BusinessLogic.Application.SqlHelper)
                {
                    //var query = @"SELECT * FROM EntryComment WHERE Status = @Status";
                    var query = string.Empty;
                    umbraco.DataLayer.IRecordsReader reader = null;
                    if (competitionNodeId == 0)
                    {
                        query = @"SELECT * FROM EntryComment WHERE Status = @Status";
                        reader = sqlHelper.ExecuteReader(query,
                    sqlHelper.CreateParameter("@Status", status));
                    }
                    else
                    {
                        query = @"SELECT DISTINCT EC.* FROM EntryComment EC
                            INNER JOIN [dbo].[umbracoNode] UN1 
                                ON EC.EntryId = UN1.Id  
                            INNER JOIN [dbo].[umbracoNode] UN2
                                ON UN1.parentID = UN2.id
                            INNER JOIN [dbo].[umbracoNode] UN3  
                                ON UN2.parentID = @CompetitionNodeId AND EC.Status = @Status";
                        reader = sqlHelper.ExecuteReader(query,
                            sqlHelper.CreateParameter("@CompetitionNodeId", competitionNodeId),
                            sqlHelper.CreateParameter("@Status", status));
                    }                    

                    while (reader.Read())
                    {
                        comments.Add(new Comment
                        {
                            PkCommentId = reader.GetInt("PkCommentId"),
                            EntryId = reader.GetInt("EntryId"),
                            FirstName = reader.GetString("FirstName"),
                            LastName = reader.GetString("LastName"),
                            CommentDescription = reader.GetString("Comment"),
                            DateCreated = reader.GetDateTime("DateCreated"),
                            Email = reader.GetString("Email"),
                            Status = reader.GetInt("Status"),
                            EntriesUrl = reader.GetString("EntriesUrl"),
                            UserPicture = reader.GetString("UserPicture")
                        });
                    }

                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "Mutti: Error while Getting Comment " + ex.StackTrace);
            }

            return comments;
        }

    }
}
