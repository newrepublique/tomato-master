﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace wwwroot.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult InternalError()
        {
            //re-publish and update site cache 
            Server.ScriptTimeout = 100000;
            umbraco.cms.businesslogic.web.Document.RePublishAll();
            umbraco.library.RefreshContent();

            return View();
        }
    }
}
