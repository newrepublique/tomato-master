﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wwwroot.Models
{
    public class Comment
    {
        public int PkCommentId { get; set; }
        public string CommentDescription { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime DateCreated { get; set; }
        public int EntryId { get; set; }
        public int Status { get; set; }
        public string EntriesUrl { get; set; }
        public string UserPicture { get; set; }
    }
}