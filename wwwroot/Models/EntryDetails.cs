﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace wwwroot.Models
{
    public class EntryDetails
    {
        public string entryName { get; set; }
        public string memberId { get; set; }
        public string entryType { get; set; }
        public string entryDescription { get; set; }
        public string entryUrl { get; set; }
    }

    public class CompetitionEntry
    {
        public int Id { get; set; }
        public int Views { get; set; }
        public int Shares { get; set; }
        public int Likes { get; set; }
    }
}